//MTLG.getSettings().default.ID
MTLG.loadSettings({
  default: {
    color_p1: 'yellow',
    color_p2: 'yellow',
    color_p3: 'yellow',
    color_p4: 'yellow',
    main_color: 'red',
    secondary_color: 'blue',
    tertiary_color: 'green',
    dragLimit: 10,
    timeLimit: 5,
    paintColor: 'red',
    toPaintColor: 'white',
    framerate: 80,
    border_size: 20,
    speed: 8,
    ball_color: 'red',
    ball_size: 25,
    ball_spawnrate: 2,
    balls_start: 1,
    win_limit: 200,
    stepsizeFactor: 15,
    drawingTime: 8000
  },
});
