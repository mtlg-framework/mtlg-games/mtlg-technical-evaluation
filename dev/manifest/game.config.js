/**
 * @Date:   2019-03-06T15:07:33+01:00
 * @Last modified time: 2019-07-03T19:32:48+02:00
 */



MTLG.loadOptions({
   "login":{
      login_required: true,
      input_method: "virtual"
    },
  "width": 1920, //game width in pixels
  "height": 1080, //game height in pixels
  "languages": ["en"], //Supported languages. First language should be the most complete.
  "countdown": 180, //idle time countdown
  "fps": "60", //Frames per second
  "playernumber": 4, //Maximum number of supported players
  "FilterTouches": true, //Tangible setting: true means a tangible is recognized as one touch event. False: 4 events.
  "webgl": false, //Using webgl enables using more graphical effects
  "overlayMenu": false, //Starting the overlay menu automatically
  "offlineurl": "localhost:8080",
  "url": "http://lernspiele.informatik.rwth-aachen.de/games/mtlg-technical-evaluation",
  "xapi": { //Configurations for the xapidatacollector. Endpoint and auth properties are required for the xapidatacollector module to work.
    "endpoint": "http://lrs.elearn.rwth-aachen.de/data/xAPI/", //The endpoint of the LRS where XAPI statements are send
    "auth": "Basic M2NiNGQzZDBjODdhOWNlYzJiOTUyNWFkYjUxNWU5NjU0MTg1YTlkZjpiNzcxYTE1ZTU3ZmI1Zjg2YTAxMGU4OGY0ZGZmNDBmYzkyOWQ4OWE4", //Authentification token for LRS
    "gamename": "technical-evaluation", //The name of the game can be transmitted as the platform attribute
    "actor": { //A default actor can be specified to be used when no actor is present for a statement
      objectType: "Agent",
      id: "mailto:defaultActor@test.com",
      name: "DefaultActor"
    }
  }

});
