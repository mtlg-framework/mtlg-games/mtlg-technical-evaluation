
// expose framework for debugging
window.MTLG = MTLG;
// ignite the framework
document.addEventListener("DOMContentLoaded", MTLG.init);

// add moduls
require('mtlg-modul-menu');
require('mtlg-moduls-utilities');
require('mtlg-modul-lightparse');
require("mtlg-module-login");
require("mtlg-module-dd");
require("mtlg-modul-xapidatacollector");

// load configuration
require('../manifest/device.config.js');
require('../manifest/game.config.js');
require('../manifest/game.settings.js');

// load translations
require('../lang/lang.js');

// load axios
const axios = require('axios').default;

//load CKAN library
//var CKAN = require('ckan');

/**
 * Init function for the game.
 * @params options: The options parsed from the manifest files/ Defaults.
 */
var initGame = function (pOptions) {

  //Initialize game

  //Initialize levels and Menus:
  //The MTLG framework uses the lifecycle manager (lc) to handle level progression,
  //resetting levels and to define a starting point for the game.
  //Use the functions registerMenu and registerLevel to define your levels.
  //The starting point of the game will be the registered menu, if one is present,
  //or the game with the highest value when passed the empty game state.
  //Register Menu

  MTLG.lc.registerMenu(drawMainMenu);

  //Register levels
  //Levels consist of two parts: the first parameter is the entrance function,
  //that sets the level up and renders it. The second parameter as a check function,
  //that receives the current gamestate as paramter and returns a value between 0 and 1.
  //The lifecycle manager starts the level with the highest return value in the next step.

  // Register Basic Level
  MTLG.lc.registerLevel(login_init, checkLogin);
  MTLG.lc.registerLevel(firstlevel_init, checkLevel1);
  MTLG.lc.registerLevel(secondlevel_init, checkLevel2);
  MTLG.lc.registerLevel(thirdlevel_init, checkLevel3);
  //MTLG.lc.registerLevel(fourthlevel_init, checkLevel4);
  MTLG.lc.registerLevel(pong_login_init, checkPong);
  MTLG.lc.registerLevel(control_init, checkControl);
  //MTLG.lc.registerLevel(logEvent, checkLogger);
  MTLG.lc.registerLevel(ckan_init, checkCKAN);

  //Init is done
  console.log("Game Init function called");
}

//Register Init function with the MTLG framework
//The function passed to addGameInit will be used to initialize the game.
//Use the initialization to register levels and menus.
MTLG.addGameInit(initGame);
