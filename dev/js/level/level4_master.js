function qr_code_init() {
  var room_name = 'pong' + Math.floor(Math.random() * 100000)//or passed name
  var url = MTLG.getOptions().url + "/?room=" + room_name //MTLG.getOptions().offlineurl for tests via localhost:8080, otherwise MTLG.getOptions().url

  console.log(room_name);
  console.log(url);

  MTLG.utils.qrcode.createQRCode(url, (obj) => {
    obj.bitmap.x = MTLG.getOptions().width / 2 - MTLG.getOptions().height / 10
    obj.bitmap.y = MTLG.getOptions().height * 4 / 10
    //obj.bitmap.textObj.text.color = 'black'
    //console.log(obj.bitmap.textObj.text)
    MTLG.getStageContainer().addChild(obj.bitmap)
    //remove listener
    pong_init(obj.bitmap, room_name)
  }, { width: MTLG.getOptions().height / 5, showText: false, fontColor: 'white' })
}

function pong_init(qrcode, room_name) {
  MTLG.distributedDisplays.rooms.createRoom(room_name, function (result) {
    if (result && result.success) {
      var [player1, player2] = drawPong(qrcode); //destructuring from EcmaScript2015, maybe we need to do it oldschool (and ugly)
      //pong_init_room(player1, player2);
    } else {
      MTLG.distributedDisplays.rooms.joinRoom(room_name, function (result) {
        if (result && result.success) {
          //I think it makes more sense that we retry if the room already exists
          MTLG.getStageContainer().removeChild(qrcode)
          qr_code_init()
        } else {
          console.error("Something went wrong in creating or joining the room for pong");
        }
      })
    }
  });
}

function checkPong(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel === "pong") {
    return 1;
  }
  return 0;
}

function pong_login_init() {
  var stage = MTLG.getStageContainer();
  var player1 = false;
  var player2 = false;
  var player3 = false;
  var player4 = false;

  //Not everybody logged in. But now please.
  if (!MTLG.getPlayer(3)) {
    MTLG.login.showLogin();
    var readyButton1 = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
    readyButton1.regX = 100 / 2;
    readyButton1.regY = 100 / 2;
    readyButton1.rotation = 180;
    readyButton1.x = MTLG.getOptions().width / 2 - 210;
    readyButton1.y = MTLG.getOptions().height / 2 - 50;
    stage.addChild(readyButton1);

    readyButton1.on("pressup", function (evt) {
      readyButton1.text = "";
      player1 = true;
      logEvent("voted", 1)
      if (player1 && player2 && player3 && player4) {
        MTLG.login.hideLogin();
        logEvent("triggered", 1);
        stage.removeChild(readyButton1);
        stage.removeChild(readyButton2);
        stage.removeChild(readyButton3);
        stage.removeChild(readyButton4);
        qr_code_init();
      }
    });


    var readyButton2 = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
    readyButton2.regX = 100 / 2;
    readyButton2.regY = 100 / 2;
    readyButton2.rotation = 180;
    readyButton2.x = MTLG.getOptions().width / 2 + 750;
    readyButton2.y = MTLG.getOptions().height / 2 - 50;
    stage.addChild(readyButton2);

    readyButton2.on("pressup", function (evt) {
      readyButton2.text = "";
      player2 = true;
      logEvent("voted", 2)
      if (player1 && player2 && player3 && player4) {
        MTLG.login.hideLogin();
        logEvent("triggered", 2);
        stage.removeChild(readyButton1);
        stage.removeChild(readyButton2);
        stage.removeChild(readyButton3);
        stage.removeChild(readyButton4);
        qr_code_init();
      }
    });


    var readyButton3 = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
    readyButton3.regX = 100 / 2;
    readyButton3.regY = 100 / 2;
    readyButton3.rotation = 0;
    readyButton3.x = MTLG.getOptions().width / 2 - 750;
    readyButton3.y = MTLG.getOptions().height / 2 + 40;
    stage.addChild(readyButton3);

    readyButton3.on("pressup", function (evt) {
      readyButton3.text = "";
      player3 = true;
      logEvent("voted", 3)
      if (player1 && player2 && player3 && player4) {
        MTLG.login.hideLogin();
        logEvent("triggered", 3);
        stage.removeChild(readyButton1);
        stage.removeChild(readyButton2);
        stage.removeChild(readyButton3);
        stage.removeChild(readyButton4);
        qr_code_init();
      }
    });


    var readyButton4 = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
    readyButton4.regX = 100 / 2;
    readyButton4.regY = 100 / 2;
    readyButton4.rotation = 0;
    readyButton4.x = MTLG.getOptions().width / 2 + 210;
    readyButton4.y = MTLG.getOptions().height / 2 + 40;
    stage.addChild(readyButton4);

    readyButton4.on("pressup", function (evt) {
      readyButton4.text = "";
      player4 = true;
      logEvent("voted", 4)
      if (player1 && player2 && player3 && player4) {
        MTLG.login.hideLogin();
        logEvent("triggered", 4);
        stage.removeChild(readyButton1);
        stage.removeChild(readyButton2);
        stage.removeChild(readyButton3);
        stage.removeChild(readyButton4);
        qr_code_init();
      }
    });
  }
  else {
    qr_code_init();
  }
}


function drawPong(qrcode) {
  console.log("Master created.")
  var stage = MTLG.getStageContainer();
  var ball_spawnrate = MTLG.getSettings().default.ball_spawnrate

  //Local level Button
  var Local = new createjs.Text("Local", "bold 40px Lucida Console", MTLG.getSettings().default.main_color);
  Local.x = MTLG.getOptions().width / 2 - 675;
  Local.y = MTLG.getOptions().height / 2 + 340;
  Local.rotation = 0;
  stage.addChild(Local);

  var b1 = new createjs.Shape();
  b1.graphics.beginFill(MTLG.getSettings().default.color_p1).drawRect(0, 0, 100, 100);
  b1.regX = 100 / 2;
  b1.regY = 100 / 2;
  b1.x = MTLG.getOptions().width / 2 - 500;
  b1.y = MTLG.getOptions().height / 2 - 400;
  b1.rotation = 0;

  var b2 = new createjs.Shape();
  b2.graphics.beginFill(MTLG.getSettings().default.color_p2).drawRect(0, 0, 100, 100);
  b2.regX = 100 / 2;
  b2.regY = 100 / 2;
  b2.x = MTLG.getOptions().width / 2 + 500;
  b2.y = MTLG.getOptions().height / 2 - 400;
  b2.rotation = 0;

  var b3 = new createjs.Shape();
  b3.graphics.beginFill(MTLG.getSettings().default.color_p3).drawRect(0, 0, 100, 100);
  b3.regX = 100 / 2;
  b3.regY = 100 / 2;
  b3.x = MTLG.getOptions().width / 2 - 500;
  b3.y = MTLG.getOptions().height / 2 + 400;
  b3.rotation = 0;

  var b4 = new createjs.Shape();
  b4.graphics.beginFill(MTLG.getSettings().default.color_p4).drawRect(0, 0, 100, 100);
  b4.regX = 100 / 2;
  b4.regY = 100 / 2;
  b4.x = MTLG.getOptions().width / 2 + 500;
  b4.y = MTLG.getOptions().height / 2 + 400;
  b4.rotation = 0;

  Local.on("pressup", function (evt) {
    stage.addChild(b1);
    stage.addChild(b2);
    stage.addChild(b3);
    stage.addChild(b4);
    stage.removeChild(Local);
    stage.removeChild(qrcode);

    for (i = 0; i < MTLG.getSettings().default.balls_start; i++) {
      new Ball(stage, collision_elements);
    }
  });

  //Pong controller for offline games
  b1.on("pressup", function (evt) {
    player1_bar.y -= MTLG.getOptions().height / MTLG.getSettings().default.stepsizeFactor;
    player1_bar.setBounds(0.05 * MTLG.getOptions().width, player1_bar.y + 540, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
    logEvent("moved", 1);
    console.log("Player 0, Up");
    //console.log(player1_bar.getBounds())
  });

  b2.on("pressup", function (evt) {
    player2_bar.y -= MTLG.getOptions().height / MTLG.getSettings().default.stepsizeFactor;
    player2_bar.setBounds(0.95 * MTLG.getOptions().width, player2_bar.y + 540, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
    logEvent("moved", 3);
    console.log("Player 2, Up");
    //console.log(player2_bar.getBounds())
  });

  b3.on("pressup", function (evt) {
    player1_bar.y += MTLG.getOptions().height / MTLG.getSettings().default.stepsizeFactor;
    player1_bar.setBounds(0.05 * MTLG.getOptions().width, player1_bar.y + 540, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
    logEvent("moved", 2);
    console.log("Player 1, Down");
    //console.log(player1_bar.getBounds())
  });

  b4.on("pressup", function (evt) {
    player2_bar.y += MTLG.getOptions().height / MTLG.getSettings().default.stepsizeFactor;
    player2_bar.setBounds(0.95 * MTLG.getOptions().width, player2_bar.y + 540, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
    logEvent("moved", 4);
    console.log("Player 3, Down");
    //console.log(player2_bar.getBounds())
  });

  //Pong controller for online games
  MTLG.distributedDisplays.actions.setCustomFunction('getPlayers', function (requester) {
    MTLG.distributedDisplays.communication.sendCustomAction(requester, 'receivePlayers', MTLG.getPlayers());
  });

  MTLG.distributedDisplays.actions.setCustomFunction('moveBar', function (data) {
    console.log("Data from controll recieved.")
    if (data.player && data.player.position == 1) {
      player1_bar.y -= MTLG.getOptions().height / MTLG.getSettings().default.stepsizeFactor;
      player1_bar.setBounds(0.05 * MTLG.getOptions().width, player1_bar.y + 540, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
      logEvent("moved", 1);
      console.log("Player 0, Up");
      //console.log(player1_bar.getBounds())
    }
    else if (data.player && data.player.position == 2) {
      player2_bar.y -= MTLG.getOptions().height / MTLG.getSettings().default.stepsizeFactor;
      player2_bar.setBounds(0.95 * MTLG.getOptions().width, player2_bar.y + 540, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
      logEvent("moved", 3);
      console.log("Player 2, Up");
      //console.log(player2_bar.getBounds())
    }
    else if (data.player && data.player.position == 3) {
      player1_bar.y += MTLG.getOptions().height / MTLG.getSettings().default.stepsizeFactor;
      player1_bar.setBounds(0.05 * MTLG.getOptions().width, player1_bar.y + 540, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
      logEvent("moved", 2);
      console.log("Player 1, Down");
      //console.log(player1_bar.getBounds())
    }
    else if (data.player && data.player.position == 4) {
      player2_bar.y += MTLG.getOptions().height / MTLG.getSettings().default.stepsizeFactor;
      player2_bar.setBounds(0.95 * MTLG.getOptions().width, player2_bar.y + 540, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
      logEvent("moved", 4);
      console.log("Player 3, Down");
      //console.log(player2_bar.getBounds())
    }
  });

  //Create Field
  var left = new createjs.Shape();
  left.graphics.beginFill(MTLG.getSettings().default.color_p2).drawRect(-MTLG.getSettings().default.border_size, 0, MTLG.getSettings().default.border_size, MTLG.getOptions().height);
  left.setBounds(-MTLG.getSettings().default.border_size, 0, MTLG.getSettings().default.border_size, MTLG.getOptions().height)

  var right = new createjs.Shape();
  right.graphics.beginFill(MTLG.getSettings().default.color_p4).drawRect(MTLG.getOptions().width, 0, MTLG.getSettings().default.border_size, MTLG.getOptions().height)
  right.setBounds(MTLG.getOptions().width, 0, MTLG.getSettings().default.border_size, MTLG.getOptions().height)

  var top = new createjs.Shape();
  top.graphics.beginFill(MTLG.getSettings().default.color_p1).drawRect(0, 0, MTLG.getOptions().width, MTLG.getSettings().default.border_size);
  top.setBounds(0, 0, MTLG.getOptions().width, MTLG.getSettings().default.border_size);

  var down = new createjs.Shape();
  down.graphics.beginFill(MTLG.getSettings().default.color_p3).drawRect(0, MTLG.getOptions().height - MTLG.getSettings().default.border_size, MTLG.getOptions().width, MTLG.getSettings().default.border_size);
  down.setBounds(0, MTLG.getOptions().height - MTLG.getSettings().default.border_size, MTLG.getOptions().width, MTLG.getSettings().default.border_size);

  //Create Players
  var player1 = new createjs.Container();

  var player1_bar = new createjs.Shape();
  player1_bar.graphics.beginFill(MTLG.getSettings().default.color_p1).drawRect(0.05 * MTLG.getOptions().width, 0.5 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
  player1_bar.setBounds(0.05 * MTLG.getOptions().width, 0.5 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height)

  var player1_top = new createjs.Shape();
  player1_top.graphics.beginFill(MTLG.getSettings().default.color_p1).drawRect(0.05 * MTLG.getOptions().width, 0.24 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.01 * MTLG.getOptions().height);
  player1_top.setBounds(0.05 * MTLG.getOptions().width, 0.24 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.01 * MTLG.getOptions().height);

  var player1_bot = new createjs.Shape();
  player1_bot.graphics.beginFill(MTLG.getSettings().default.color_p2).drawRect(0.05 * MTLG.getOptions().width, 0.35 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.01 * MTLG.getOptions().height);
  player1_bot.setBounds(0.05 * MTLG.getOptions().width, 0.35 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.01 * MTLG.getOptions().height);


  var player2 = new createjs.Container();

  var player2_bar = new createjs.Shape();
  player2_bar.graphics.beginFill(MTLG.getSettings().default.color_p3).drawRect(0.95 * MTLG.getOptions().width, 0.5 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height);
  player2_bar.setBounds(0.95 * MTLG.getOptions().width, 0.5 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.25 * MTLG.getOptions().height)

  var player2_top = new createjs.Shape();
  player2_top.graphics.beginFill(MTLG.getSettings().default.color_p4).drawRect(0.95 * MTLG.getOptions().width, 0.24 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.01 * MTLG.getOptions().height);
  player2_top.setBounds(0.95 * MTLG.getOptions().width, 0.24 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.01 * MTLG.getOptions().height);

  var player2_bot = new createjs.Shape();
  player2_bot.graphics.beginFill(MTLG.getSettings().default.color_p4).drawRect(0.95 * MTLG.getOptions().width, 0.35 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.01 * MTLG.getOptions().height);
  player2_bot.setBounds(0.95 * MTLG.getOptions().width, 0.35 * MTLG.getOptions().height, MTLG.getSettings().default.border_size, 0.01 * MTLG.getOptions().height);

  /*
  var player2_bar = new createjs.Shape();
  player2_bar.graphics.beginFill("grey").drawRect(0.95 * MTLG.getOptions().width, 0.25 * MTLG.getOptions().height, config.border_size, 0.5 * MTLG.getOptions().height);
  player2_bar.setBounds(0.95 * MTLG.getOptions().width, 0.25 * MTLG.getOptions().height, config.border_size, 0.5 * MTLG.getOptions().height)

  var player2_top = new createjs.Shape();
  player2_top.graphics.beginFill("grey").drawRect(0.95 * MTLG.getOptions().width, 0.24 * MTLG.getOptions().height, config.border_size, 0.01 * MTLG.getOptions().height);
  player2_top.setBounds(0.95 * MTLG.getOptions().width, 0.24 * MTLG.getOptions().height, config.border_size, 0.01 * MTLG.getOptions().height);

  var player2_bot = new createjs.Shape();
  player2_bot.graphics.beginFill("grey").drawRect(0.95 * MTLG.getOptions().width, 0.75 * MTLG.getOptions().height, config.border_size, 0.01 * MTLG.getOptions().height);
  player2_bot.setBounds(0.95 * MTLG.getOptions().width, 0.75 * MTLG.getOptions().height, config.border_size, 0.01 * MTLG.getOptions().height);
  */

  //Point Counter
  var p1_scr = 0
  var p1_text = new createjs.Text(p1_scr, "100px Arial", MTLG.getSettings().default.color_p2);
  p1_text.y = 2 * MTLG.getSettings().default.border_size
  p1_text.x = 0.45 * MTLG.getOptions().width;
  p1_text.textAlign = 'center'
  function inc_p1_scr() {
    p1_text.text = ++p1_scr;
    logEvent("progressed", 1);
    logEvent("progressed", 2);
    if (p1_scr == MTLG.getSettings().default.win_limit) {
      stage.removeChild(player2);
      //player2.children.forEach((elem => { elem.setBounds(0, 0, 0, 0) }));
      player2_bar.setBounds(0, 0, 0, 0);
      ball_spawnrate = 0;
      logEvent("won", 1);
      logEvent("won", 2);
      logEvent("lost", 3);
      logEvent("lost", 4);
      console.log("Downloading Logfile..");
      setTimeout(downloadLogfile(), 3000);
      alert("Game Over! Players 1 and 2 win!");
    }
  }

  var p2_scr = 0
  var p2_text = new createjs.Text("0", "100px Arial", MTLG.getSettings().default.color_p4);
  p2_text.y = 2 * MTLG.getSettings().default.border_size
  p2_text.x = 0.55 * MTLG.getOptions().width;
  p2_text.textAlign = 'center'
  function inc_p2_scr() {
    p2_text.text = ++p2_scr;
    logEvent("progressed", 3);
    logEvent("progressed", 4);
    if (p2_scr == MTLG.getSettings().default.win_limit) {
      stage.removeChild(player1)
      //player1.children.forEach((elem => { elem.setBounds(0, 0, 0, 0) }))
      player1_bar.setBounds(0, 0, 0, 0);
      ball_spawnrate = 0
      logEvent("lost", 1);
      logEvent("lost", 2);
      logEvent("won", 3);
      logEvent("won", 4);
      console.log("Downloading Logfile..");
      setTimeout(downloadLogfile(), 3000);
      alert("Game Over! Players 3 and 4 win!");
    }
  }

  /*
  player1.addChild(player1_bar, player1_bot, player1_top)
  player2.addChild(player2_bar, player2_bot, player2_top)
  */
  player1.addChild(player1_bar)
  player2.addChild(player2_bar)
  stage.addChild(p1_text, p2_text, left, right, top, down, player1, player2)

  var collision_elements = {
    left, right, top, down, player1_bar, player1_bot, player1_top, player2_bar, player2_bot, player2_top
  }

  loggedin_players = 0
  MTLG.distributedDisplays.actions.setCustomFunction('player_selected', function (succes) {
    if (succes) {
      loggedin_players++
      if (loggedin_players >= MTLG.getPlayerNumber()) {
        console.log("LETS GOOO!")
        stage.removeChild(Local);
        stage.removeChild(qrcode)
        for (i = 0; i < MTLG.getSettings().default.balls_start; i++) {
          new Ball(stage, collision_elements)
        }
      }
    }
  });
  createjs.Ticker.framerate = MTLG.getSettings().default.framerate

  // add objects to stage
  function Ball(stage, elems) {
    this.shape = new createjs.Shape();
    this.shape.graphics.beginStroke(MTLG.getSettings().default.ball_color).beginFill(MTLG.getSettings().default.ball_color).drawCircle(0, 0, MTLG.getSettings().default.ball_size);
    this.shape.x = MTLG.getOptions().width / 2
    this.shape.y = MTLG.getOptions().height / 2
    this.shape.setBounds(this.shape.x - MTLG.getSettings().default.ball_size, this.shape.y - MTLG.getSettings().default.ball_size, 2 * MTLG.getSettings().default.ball_size, 2 * MTLG.getSettings().default.ball_size)
    this.vx = (0.5 + Math.random() / 4) * MTLG.getSettings().default.speed * Math.sign(Math.random() - 0.5)
    this.vy = (MTLG.getSettings().default.speed - Math.abs(this.vx)) * Math.sign(Math.random() - 0.5)

    stage.addChild(this.shape);


    var ball = this
    function update_frame() {

      check_collision()
      createjs.Tween.get(ball, { useTicks: true }).to({ x: Math.round(ball.shape.x + ball.vx), y: Math.round(ball.shape.y + ball.vy) }, 1)
        //createjs.Tween.get(ball).to({x: Math.round(ball.shape.x + ball.vx), y: Math.round(ball.shape.y + ball.vy)},Math.floor(1000/(MTLG.getSettings().default.framerate+1)))
        .call(() => {
          ball.shape.x += ball.vx
          ball.shape.y += ball.vy
          ball.shape.setBounds(ball.shape.x - MTLG.getSettings().default.ball_size, ball.shape.y - MTLG.getSettings().default.ball_size, 2 * MTLG.getSettings().default.ball_size, 2 * MTLG.getSettings().default.ball_size)
        })
    }
    var collision_lock = false

    //Wrapper for dynamic elements
    /*
    function getBounds_wPos(elem) {
      var bounds = elem.getBounds()
      bounds.x += elem.x
      bounds.y += elem.y
      bounds.width += elem.x
      bounds.height += elem.y
      return bounds
    }
    */

    function check_collision() {

      if (!collision_lock && MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), elems.left.getBounds())) {
        collision_lock = true
        inc_p2_scr()
        console.log("Vertical Border")
        createjs.Ticker.off("tick", listener)
        stage.removeChild(ball.shape)
        for (i = 0; i < ball_spawnrate; i++) {
          new Ball(stage, elems)
        }
        collision_lock = false
      }
      if (!collision_lock && MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), elems.right.getBounds())) {
        collision_lock = true
        inc_p1_scr()
        console.log("Vertical Border")
        createjs.Ticker.off("tick", listener)
        stage.removeChild(ball.shape)
        for (i = 0; i < ball_spawnrate; i++) {
          new Ball(stage, elems)
        }

        collision_lock = false
      }

      //Horizontale Kanten, should have iterated here but elements were added one after another and how about an endless line?
      //if (MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), elems.top.getBounds()) || MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), elems.down.getBounds()) || MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), getBounds_wPos(elems.player1_top)) || MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), getBounds_wPos(elems.player1_bot)) || MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), getBounds_wPos(elems.player2_top)) || MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), getBounds_wPos(elems.player2_bot))) {
      if (MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), elems.top.getBounds()) || MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), elems.down.getBounds())) {
        console.log("Horizontal Border")
        ball.vy *= -1
      }

      //Vertikale Kanten der Spieler
      //console.log(getBounds_wPos(player1_bar));
      //if (MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), getBounds_wPos(player1_bar)) || MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), getBounds_wPos(player2_bar))) {
      if (MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), player1_bar.getBounds()) || MTLG.utils.collision.calculateIntersection(ball.shape.getBounds(), player2_bar.getBounds())) {
        console.log("Player Border Vertical")
        ball.vx *= -1
      }
    }

    var listener = createjs.Ticker.on("tick", update_frame);
  }

  return [player1, player2]
}
