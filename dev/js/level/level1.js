
function firstlevel_init() {
  console.log("These players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }

  console.log("These are the available game options:");
  console.log(MTLG.getOptions());

  console.log("These are the available game settings:");
  console.log(MTLG.getSettings());

  drawLevel1();
}

function checkLevel1(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "level1") {
    return 1;
  }
  return 0;
}


function drawLevel1() {
  var stage = MTLG.getStageContainer();
  console.log("Level 1 started.");

  var targethit1 = 0;
  var targethit2 = 0;
  var targethit3 = 0;
  var targethit4 = 0;

  //Skip level Button
  var skip = new createjs.Text("Skip", "bold 40px Lucida Console", MTLG.getSettings().default.main_color);
  skip.x = MTLG.getOptions().width / 2 - 675;
  skip.y = MTLG.getOptions().height / 2 + 340;
  skip.rotation = 90;
  stage.addChild(skip);

  skip.on("pressup", function (evt) {
    MTLG.lc.levelFinished({
      nextLevel: "level2"
    });
  });

  //target object
  var target = new createjs.Shape();
  target.graphics.beginFill(MTLG.getSettings().default.main_color).drawRect(0, 0, 1500, 400);
  target.regX = 1500 / 2;
  target.regY = 400 / 2;
  target.rotation = 0;
  target.x = MTLG.getOptions().width / 2;
  target.y = MTLG.getOptions().height / 2;
  stage.addChild(target);

  //counter
  var counter1 = new createjs.Text("0", "bold 60px Lucida Console", MTLG.getSettings().default.main_color);
  counter1.x = MTLG.getOptions().width / 2 - 300;
  counter1.y = MTLG.getOptions().height / 2 - 380;
  counter1.rotation = 180;
  stage.addChild(counter1);

  var counter2 = new createjs.Text("0", "bold 60px Lucida Console", MTLG.getSettings().default.main_color);
  counter2.x = MTLG.getOptions().width / 2 + 300;
  counter2.y = MTLG.getOptions().height / 2 - 380;
  counter2.rotation = 180;
  stage.addChild(counter2);

  var counter3 = new createjs.Text("0", "bold 60px Lucida Console", MTLG.getSettings().default.main_color);
  counter3.x = MTLG.getOptions().width / 2 - 300;
  counter3.y = MTLG.getOptions().height / 2 + 375;
  stage.addChild(counter3);

  var counter4 = new createjs.Text("0", "bold 60px Lucida Console", MTLG.getSettings().default.main_color);
  counter4.x = MTLG.getOptions().width / 2 + 300;
  counter4.y = MTLG.getOptions().height / 2 + 375;
  stage.addChild(counter4);

  //first dragable object
  var dragable1 = new createjs.Shape();
  dragable1.graphics.beginFill(MTLG.getSettings().default.color_p1).drawRect(0, 0, 200, 200);
  dragable1.regX = 200 / 2;
  dragable1.regY = 200 / 2;
  dragable1.rotation = 0;
  dragable1.x = MTLG.getOptions().width / 2 - 500;
  dragable1.y = MTLG.getOptions().height / 2 - 400;
  stage.addChild(dragable1);

  dragable1.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable1.set(dragable1.localToLocal(localX, localY, dragable1.parent));
  });

  dragable1.on("pressup", function (evt) {
    logEvent("dragged", 1);
    var distanceX = Math.abs(dragable1.x - target.x);
    var distanceY = Math.abs(dragable1.y - target.y);
    if ((distanceX <= 650) && (distanceY <= 100)) {
      logEvent("progressed", 1);
      targethit1++;
      counter1.text = targethit1;
      if (targethit1 == MTLG.getSettings().default.dragLimit) {
        logEvent("completed", 1);
        stage.removeChild(dragable1);
        counter1.color = 'green';
        if (targethit2 == MTLG.getSettings().default.dragLimit && targethit3 == MTLG.getSettings().default.dragLimit && targethit4 == MTLG.getSettings().default.dragLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level2"
          });
        }
      }
      dragable1.x = MTLG.getOptions().width / 2 - 500;
      dragable1.y = MTLG.getOptions().height / 2 - 400;
      console.log(targethit1);
      console.log(targethit2);
      console.log(targethit3);
      console.log(targethit4);
    }
  });

  //second dragable object
  var dragable2 = new createjs.Shape();
  dragable2.graphics.beginFill(MTLG.getSettings().default.color_p2).drawRect(0, 0, 200, 200);
  dragable2.regX = 200 / 2;
  dragable2.regY = 200 / 2;
  dragable2.rotation = 0;
  dragable2.x = MTLG.getOptions().width / 2 + 500;
  dragable2.y = MTLG.getOptions().height / 2 - 400;
  stage.addChild(dragable2);

  dragable2.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable2.set(dragable2.localToLocal(localX, localY, dragable2.parent));
  });

  dragable2.on("pressup", function (evt) {
    logEvent("dragged", 2);
    var distanceX = Math.abs(dragable2.x - target.x);
    var distanceY = Math.abs(dragable2.y - target.y);
    if ((distanceX <= 650) && (distanceY <= 100)) {
      logEvent("progressed", 2);
      targethit2++;
      counter2.text = targethit2;
      if (targethit2 == MTLG.getSettings().default.dragLimit) {
        logEvent("completed", 2);
        stage.removeChild(dragable2);
        counter2.color = 'green';
        if (targethit1 == MTLG.getSettings().default.dragLimit && targethit3 == MTLG.getSettings().default.dragLimit && targethit4 == MTLG.getSettings().default.dragLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level2"
          });
        }
      }
      dragable2.x = MTLG.getOptions().width / 2 + 500;
      dragable2.y = MTLG.getOptions().height / 2 - 400;
      console.log(targethit1);
      console.log(targethit2);
      console.log(targethit3);
      console.log(targethit4);
    }
  });

  //third dragable object
  var dragable3 = new createjs.Shape();
  dragable3.graphics.beginFill(MTLG.getSettings().default.color_p3).drawRect(0, 0, 200, 200);
  dragable3.regX = 200 / 2;
  dragable3.regY = 200 / 2;
  dragable3.rotation = 0;
  dragable3.x = MTLG.getOptions().width / 2 - 500;
  dragable3.y = MTLG.getOptions().height / 2 + 400;
  stage.addChild(dragable3);

  dragable3.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable3.set(dragable3.localToLocal(localX, localY, dragable3.parent));
  });

  dragable3.on("pressup", function (evt) {
    logEvent("dragged", 3);
    var distanceX = Math.abs(dragable3.x - target.x);
    var distanceY = Math.abs(dragable3.y - target.y);
    if ((distanceX <= 650) && (distanceY <= 100)) {
      logEvent("progressed", 3);
      targethit3++;
      counter3.text = targethit3;
      if (targethit3 == MTLG.getSettings().default.dragLimit) {
        logEvent("completed", 3);
        stage.removeChild(dragable3);
        counter3.color = 'green';
        if (targethit1 == MTLG.getSettings().default.dragLimit && targethit2 == MTLG.getSettings().default.dragLimit && targethit4 == MTLG.getSettings().default.dragLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level2"
          });
        }
      }
      dragable3.x = MTLG.getOptions().width / 2 - 500;
      dragable3.y = MTLG.getOptions().height / 2 + 400;
      console.log(targethit1);
      console.log(targethit2);
      console.log(targethit3);
      console.log(targethit4);
    }
  });

  //fourth dragable object
  var dragable4 = new createjs.Shape();
  dragable4.graphics.beginFill(MTLG.getSettings().default.color_p4).drawRect(0, 0, 200, 200);
  dragable4.regX = 200 / 2;
  dragable4.regY = 200 / 2;
  dragable4.rotation = 0;
  dragable4.x = MTLG.getOptions().width / 2 + 500;
  dragable4.y = MTLG.getOptions().height / 2 + 400;
  stage.addChild(dragable4);

  dragable4.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable4.set(dragable4.localToLocal(localX, localY, dragable4.parent));
  });

  dragable4.on("pressup", function (evt) {
    logEvent("dragged", 4);
    var distanceX = Math.abs(dragable4.x - target.x);
    var distanceY = Math.abs(dragable4.y - target.y);
    if ((distanceX <= 650) && (distanceY <= 100)) {
      logEvent("progressed", 4);
      targethit4++;
      counter4.text = targethit4;
      if (targethit4 == MTLG.getSettings().default.dragLimit) {
        logEvent("completed", 4);
        stage.removeChild(dragable4);
        counter4.color = 'green';
        if (targethit1 == MTLG.getSettings().default.dragLimit && targethit2 == MTLG.getSettings().default.dragLimit && targethit3 == MTLG.getSettings().default.dragLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level2"
          });
        }
      }
      dragable4.x = MTLG.getOptions().width / 2 + 500;
      dragable4.y = MTLG.getOptions().height / 2 + 400;
      console.log(targethit1);
      console.log(targethit2);
      console.log(targethit3);
      console.log(targethit4);
    }
  });

}
