
function secondlevel_init() {
  console.log("These players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }

  console.log("These are the available game options:");
  console.log(MTLG.getOptions());

  console.log("These are the available game settings:");
  console.log(MTLG.getSettings());

  drawLevel2();
}

function checkLevel2(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "level2") {
    return 1;
  }
  return 0;
}

function drawLevel2() {
  var stage = MTLG.getStageContainer();

  console.log("Level 2 started.");

  var timeRunning = false;
  var timeStart = 0;
  var timeEnd = 0;
  var user1 = 0;
  var user2 = 0;
  var user3 = 0;
  var user4 = 0;

  //Skip level Button
  var skip = new createjs.Text("Skip", "bold 40px Lucida Console", MTLG.getSettings().default.main_color);
  skip.x = MTLG.getOptions().width / 2 - 675;
  skip.y = MTLG.getOptions().height / 2 + 340;
  skip.rotation = 90;
  stage.addChild(skip);

  skip.on("pressup", function (evt) {
    MTLG.lc.levelFinished({
      nextLevel: "level3"
    });
  });


  var counter1 = new createjs.Text("🠗", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  counter1.x = MTLG.getOptions().width / 2 - 570;
  counter1.y = MTLG.getOptions().height / 2 - 250;
  counter1.rotation = 0;
  stage.addChild(counter1);

  var counter2 = new createjs.Text("🠖", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  counter2.x = MTLG.getOptions().width / 2 - 280;
  counter2.y = MTLG.getOptions().height / 2 - 250;
  counter2.rotation = 0;
  stage.addChild(counter2);

  //first target object
  var target1 = new createjs.Shape();
  target1.graphics.beginFill(MTLG.getSettings().default.main_color).drawRect(0, 0, 500, 300);
  target1.regX = 500 / 2;
  target1.regY = 300 / 2;
  target1.rotation = 0;
  target1.x = MTLG.getOptions().width / 2 - 400;
  target1.y = MTLG.getOptions().height / 2;
  stage.addChild(target1);

  //second target object
  var target2 = new createjs.Shape();
  target2.graphics.beginFill(MTLG.getSettings().default.main_color).drawRect(0, 0, 500, 300);
  target2.regX = 500 / 2;
  target2.regY = 300 / 2;
  target2.rotation = 0;
  target2.x = MTLG.getOptions().width / 2 + 400;
  target2.y = MTLG.getOptions().height / 2;
  stage.addChild(target2);



  //user 1 - first object
  var dragable1a = new createjs.Shape();
  dragable1a.graphics.beginFill(MTLG.getSettings().default.color_p1).drawRect(0, 0, 200, 200);
  dragable1a.regX = 200 / 2;
  dragable1a.regY = 200 / 2;
  dragable1a.rotation = 0;
  dragable1a.x = MTLG.getOptions().width / 2 - 550;
  dragable1a.y = MTLG.getOptions().height / 2 - 400;
  //dragable1a.text = "TEST";
  stage.addChild(dragable1a);

  dragable1a.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable1a.set(dragable1a.localToLocal(localX, localY, dragable1a.parent));
  });
  dragable1a.on("pressup", function (evt) {
    logEvent("dragged", 1);
    var distanceX = Math.abs(dragable1a.x - target1.x);
    var distanceY = Math.abs(dragable1a.y - target1.y);
    if ((distanceX <= 150) && (distanceY <= 50)) {
      logEvent("progressed", 1);
      stage.removeChild(dragable1a);
      user1++;
      if (!timeRunning) {
        stage.removeChild(counter1);
        stage.removeChild(counter2);
        timeRunning = true;
        timeStart = new Date();
      }
      if (user1 == 2 && user2 == 2 && user3 == 2 && user4 == 2) {
        timeEnd = new Date();
        if (((timeEnd - timeStart) / 1000) <= MTLG.getSettings().default.timeLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level3"
          });
        }
        else {
          logEvent("failed", 1);
          timeRunning = false;
          user1 = 0;
          user2 = 0;
          user3 = 0;
          user4 = 0;

          dragable1a.x = MTLG.getOptions().width / 2 - 550;
          dragable1a.y = MTLG.getOptions().height / 2 - 400;
          dragable1b.x = MTLG.getOptions().width / 2 - 250;
          dragable1b.y = MTLG.getOptions().height / 2 - 400;
          dragable2a.x = MTLG.getOptions().width / 2 + 550;
          dragable2a.y = MTLG.getOptions().height / 2 - 400;
          dragable2b.x = MTLG.getOptions().width / 2 + 250;
          dragable2b.y = MTLG.getOptions().height / 2 - 400;
          dragable3a.x = MTLG.getOptions().width / 2 - 550;
          dragable3a.y = MTLG.getOptions().height / 2 + 400;
          dragable3b.x = MTLG.getOptions().width / 2 - 250;
          dragable3b.y = MTLG.getOptions().height / 2 + 400;
          dragable4a.x = MTLG.getOptions().width / 2 + 550;
          dragable4a.y = MTLG.getOptions().height / 2 + 400;
          dragable4b.x = MTLG.getOptions().width / 2 + 250;
          dragable4b.y = MTLG.getOptions().height / 2 + 400;

          stage.addChild(dragable1a);
          stage.addChild(dragable1b);
          stage.addChild(dragable2a);
          stage.addChild(dragable2b);
          stage.addChild(dragable3a);
          stage.addChild(dragable3b);
          stage.addChild(dragable4a);
          stage.addChild(dragable4b);
          stage.addChild(counter1);
          stage.addChild(counter2);
        }
      }
    }
  });

  //user 1 - second object
  var dragable1b = new createjs.Shape();
  dragable1b.graphics.beginFill(MTLG.getSettings().default.color_p1).drawRect(0, 0, 200, 200);
  dragable1b.regX = 200 / 2;
  dragable1b.regY = 200 / 2;
  dragable1b.rotation = 0;
  dragable1b.x = MTLG.getOptions().width / 2 - 250;
  dragable1b.y = MTLG.getOptions().height / 2 - 400;
  stage.addChild(dragable1b);

  dragable1b.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable1b.set(dragable1b.localToLocal(localX, localY, dragable1b.parent));
  });
  dragable1b.on("pressup", function (evt) {
    logEvent("dragged", 1);
    var distanceX = Math.abs(dragable1b.x - target2.x);
    var distanceY = Math.abs(dragable1b.y - target2.y);
    if ((distanceX <= 150) && (distanceY <= 50)) {
      logEvent("progressed", 1);
      stage.removeChild(dragable1b);
      user1++;
      if (!timeRunning) {
        stage.removeChild(counter1);
        stage.removeChild(counter2);
        timeRunning = true;
        timeStart = new Date();
      }
      if (user1 == 2 && user2 == 2 && user3 == 2 && user4 == 2) {
        timeEnd = new Date();
        if (((timeEnd - timeStart) / 1000) <= MTLG.getSettings().default.timeLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level3"
          });
        }
        else {
          logEvent("failed", 1);
          timeRunning = false;
          user1 = 0;
          user2 = 0;
          user3 = 0;
          user4 = 0;

          dragable1a.x = MTLG.getOptions().width / 2 - 550;
          dragable1a.y = MTLG.getOptions().height / 2 - 400;
          dragable1b.x = MTLG.getOptions().width / 2 - 250;
          dragable1b.y = MTLG.getOptions().height / 2 - 400;
          dragable2a.x = MTLG.getOptions().width / 2 + 550;
          dragable2a.y = MTLG.getOptions().height / 2 - 400;
          dragable2b.x = MTLG.getOptions().width / 2 + 250;
          dragable2b.y = MTLG.getOptions().height / 2 - 400;
          dragable3a.x = MTLG.getOptions().width / 2 - 550;
          dragable3a.y = MTLG.getOptions().height / 2 + 400;
          dragable3b.x = MTLG.getOptions().width / 2 - 250;
          dragable3b.y = MTLG.getOptions().height / 2 + 400;
          dragable4a.x = MTLG.getOptions().width / 2 + 550;
          dragable4a.y = MTLG.getOptions().height / 2 + 400;
          dragable4b.x = MTLG.getOptions().width / 2 + 250;
          dragable4b.y = MTLG.getOptions().height / 2 + 400;

          stage.addChild(dragable1a);
          stage.addChild(dragable1b);
          stage.addChild(dragable2a);
          stage.addChild(dragable2b);
          stage.addChild(dragable3a);
          stage.addChild(dragable3b);
          stage.addChild(dragable4a);
          stage.addChild(dragable4b);
          stage.addChild(counter1);
          stage.addChild(counter2);
        }
      }
    }
  });


  //user 2 - first object
  var dragable2a = new createjs.Shape();
  dragable2a.graphics.beginFill(MTLG.getSettings().default.color_p2).drawRect(0, 0, 200, 200);
  dragable2a.regX = 200 / 2;
  dragable2a.regY = 200 / 2;
  dragable2a.rotation = 0;
  dragable2a.x = MTLG.getOptions().width / 2 + 550;
  dragable2a.y = MTLG.getOptions().height / 2 - 400;
  stage.addChild(dragable2a);

  dragable2a.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable2a.set(dragable2a.localToLocal(localX, localY, dragable2a.parent));
  });
  dragable2a.on("pressup", function (evt) {
    logEvent("dragged", 2);
    var distanceX = Math.abs(dragable2a.x - target2.x);
    var distanceY = Math.abs(dragable2a.y - target2.y);
    if ((distanceX <= 150) && (distanceY <= 50)) {
      logEvent("progressed", 2);
      stage.removeChild(dragable2a);
      user2++;
      if (!timeRunning) {
        stage.removeChild(counter1);
        stage.removeChild(counter2);
        timeRunning = true;
        timeStart = new Date();
      }
      if (user1 == 2 && user2 == 2 && user3 == 2 && user4 == 2) {
        timeEnd = new Date();
        if (((timeEnd - timeStart) / 1000) <= MTLG.getSettings().default.timeLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level3"
          });
        }
        else {
          logEvent("failed", 2);
          timeRunning = false;
          user1 = 0;
          user2 = 0;
          user3 = 0;
          user4 = 0;

          dragable1a.x = MTLG.getOptions().width / 2 - 550;
          dragable1a.y = MTLG.getOptions().height / 2 - 400;
          dragable1b.x = MTLG.getOptions().width / 2 - 250;
          dragable1b.y = MTLG.getOptions().height / 2 - 400;
          dragable2a.x = MTLG.getOptions().width / 2 + 550;
          dragable2a.y = MTLG.getOptions().height / 2 - 400;
          dragable2b.x = MTLG.getOptions().width / 2 + 250;
          dragable2b.y = MTLG.getOptions().height / 2 - 400;
          dragable3a.x = MTLG.getOptions().width / 2 - 550;
          dragable3a.y = MTLG.getOptions().height / 2 + 400;
          dragable3b.x = MTLG.getOptions().width / 2 - 250;
          dragable3b.y = MTLG.getOptions().height / 2 + 400;
          dragable4a.x = MTLG.getOptions().width / 2 + 550;
          dragable4a.y = MTLG.getOptions().height / 2 + 400;
          dragable4b.x = MTLG.getOptions().width / 2 + 250;
          dragable4b.y = MTLG.getOptions().height / 2 + 400;

          stage.addChild(dragable1a);
          stage.addChild(dragable1b);
          stage.addChild(dragable2a);
          stage.addChild(dragable2b);
          stage.addChild(dragable3a);
          stage.addChild(dragable3b);
          stage.addChild(dragable4a);
          stage.addChild(dragable4b);
          stage.addChild(counter1);
          stage.addChild(counter2);
        }
      }
    }
  });

  //user 2 - second object
  var dragable2b = new createjs.Shape();
  dragable2b.graphics.beginFill(MTLG.getSettings().default.color_p2).drawRect(0, 0, 200, 200);
  dragable2b.regX = 200 / 2;
  dragable2b.regY = 200 / 2;
  dragable2b.rotation = 0;
  dragable2b.x = MTLG.getOptions().width / 2 + 250;
  dragable2b.y = MTLG.getOptions().height / 2 - 400;
  stage.addChild(dragable2b);

  dragable2b.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable2b.set(dragable2b.localToLocal(localX, localY, dragable2b.parent));
  });
  dragable2b.on("pressup", function (evt) {
    logEvent("dragged", 2);
    var distanceX = Math.abs(dragable2b.x - target1.x);
    var distanceY = Math.abs(dragable2b.y - target1.y);
    if ((distanceX <= 150) && (distanceY <= 50)) {
      logEvent("progressed", 2);
      stage.removeChild(dragable2b);
      user2++;
      if (!timeRunning) {
        stage.removeChild(counter1);
        stage.removeChild(counter2);
        timeRunning = true;
        timeStart = new Date();
      }
      if (user1 == 2 && user2 == 2 && user3 == 2 && user4 == 2) {
        timeEnd = new Date();
        if (((timeEnd - timeStart) / 1000) <= MTLG.getSettings().default.timeLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level3"
          });
        }
        else {
          logEvent("failed", 2);
          timeRunning = false;
          user1 = 0;
          user2 = 0;
          user3 = 0;
          user4 = 0;

          dragable1a.x = MTLG.getOptions().width / 2 - 550;
          dragable1a.y = MTLG.getOptions().height / 2 - 400;
          dragable1b.x = MTLG.getOptions().width / 2 - 250;
          dragable1b.y = MTLG.getOptions().height / 2 - 400;
          dragable2a.x = MTLG.getOptions().width / 2 + 550;
          dragable2a.y = MTLG.getOptions().height / 2 - 400;
          dragable2b.x = MTLG.getOptions().width / 2 + 250;
          dragable2b.y = MTLG.getOptions().height / 2 - 400;
          dragable3a.x = MTLG.getOptions().width / 2 - 550;
          dragable3a.y = MTLG.getOptions().height / 2 + 400;
          dragable3b.x = MTLG.getOptions().width / 2 - 250;
          dragable3b.y = MTLG.getOptions().height / 2 + 400;
          dragable4a.x = MTLG.getOptions().width / 2 + 550;
          dragable4a.y = MTLG.getOptions().height / 2 + 400;
          dragable4b.x = MTLG.getOptions().width / 2 + 250;
          dragable4b.y = MTLG.getOptions().height / 2 + 400;

          stage.addChild(dragable1a);
          stage.addChild(dragable1b);
          stage.addChild(dragable2a);
          stage.addChild(dragable2b);
          stage.addChild(dragable3a);
          stage.addChild(dragable3b);
          stage.addChild(dragable4a);
          stage.addChild(dragable4b);
          stage.addChild(counter1);
          stage.addChild(counter2);
        }
      }
    }
  });


  //user 3 - first object
  var dragable3a = new createjs.Shape();
  dragable3a.graphics.beginFill(MTLG.getSettings().default.color_p3).drawRect(0, 0, 200, 200);
  dragable3a.regX = 200 / 2;
  dragable3a.regY = 200 / 2;
  dragable3a.rotation = 0;
  dragable3a.x = MTLG.getOptions().width / 2 - 550;
  dragable3a.y = MTLG.getOptions().height / 2 + 400;
  stage.addChild(dragable3a);

  dragable3a.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable3a.set(dragable3a.localToLocal(localX, localY, dragable3a.parent));
  });
  dragable3a.on("pressup", function (evt) {
    logEvent("dragged", 3);
    var distanceX = Math.abs(dragable3a.x - target1.x);
    var distanceY = Math.abs(dragable3a.y - target1.y);
    if ((distanceX <= 150) && (distanceY <= 50)) {
      logEvent("progressed", 3);
      stage.removeChild(dragable3a);
      user3++;
      if (!timeRunning) {
        stage.removeChild(counter1);
        stage.removeChild(counter2);
        timeRunning = true;
        timeStart = new Date();
      }
      if (user1 == 2 && user2 == 2 && user3 == 2 && user4 == 2) {
        timeEnd = new Date();
        if (((timeEnd - timeStart) / 1000) <= MTLG.getSettings().default.timeLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level3"
          });
        }
        else {
          logEvent("failed", 3);
          timeRunning = false;
          user1 = 0;
          user2 = 0;
          user3 = 0;
          user4 = 0;

          dragable1a.x = MTLG.getOptions().width / 2 - 550;
          dragable1a.y = MTLG.getOptions().height / 2 - 400;
          dragable1b.x = MTLG.getOptions().width / 2 - 250;
          dragable1b.y = MTLG.getOptions().height / 2 - 400;
          dragable2a.x = MTLG.getOptions().width / 2 + 550;
          dragable2a.y = MTLG.getOptions().height / 2 - 400;
          dragable2b.x = MTLG.getOptions().width / 2 + 250;
          dragable2b.y = MTLG.getOptions().height / 2 - 400;
          dragable3a.x = MTLG.getOptions().width / 2 - 550;
          dragable3a.y = MTLG.getOptions().height / 2 + 400;
          dragable3b.x = MTLG.getOptions().width / 2 - 250;
          dragable3b.y = MTLG.getOptions().height / 2 + 400;
          dragable4a.x = MTLG.getOptions().width / 2 + 550;
          dragable4a.y = MTLG.getOptions().height / 2 + 400;
          dragable4b.x = MTLG.getOptions().width / 2 + 250;
          dragable4b.y = MTLG.getOptions().height / 2 + 400;

          stage.addChild(dragable1a);
          stage.addChild(dragable1b);
          stage.addChild(dragable2a);
          stage.addChild(dragable2b);
          stage.addChild(dragable3a);
          stage.addChild(dragable3b);
          stage.addChild(dragable4a);
          stage.addChild(dragable4b);
          stage.addChild(counter1);
          stage.addChild(counter2);
        }
      }
    }
  });

  //user 3 - second object
  var dragable3b = new createjs.Shape();
  dragable3b.graphics.beginFill(MTLG.getSettings().default.color_p3).drawRect(0, 0, 200, 200);
  dragable3b.regX = 200 / 2;
  dragable3b.regY = 200 / 2;
  dragable3b.rotation = 0;
  dragable3b.x = MTLG.getOptions().width / 2 - 250;
  dragable3b.y = MTLG.getOptions().height / 2 + 400;
  stage.addChild(dragable3b);

  dragable3b.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable3b.set(dragable3b.localToLocal(localX, localY, dragable3b.parent));
  });
  dragable3b.on("pressup", function (evt) {
    logEvent("dragged", 3);
    var distanceX = Math.abs(dragable3b.x - target2.x);
    var distanceY = Math.abs(dragable3b.y - target2.y);
    if ((distanceX <= 150) && (distanceY <= 50)) {
      logEvent("progressed", 3);
      stage.removeChild(dragable3b);
      user3++;
      if (!timeRunning) {
        stage.removeChild(counter1);
        stage.removeChild(counter2);
        timeRunning = true;
        timeStart = new Date();
      }
      if (user1 == 2 && user2 == 2 && user3 == 2 && user4 == 2) {
        timeEnd = new Date();
        if (((timeEnd - timeStart) / 1000) <= MTLG.getSettings().default.timeLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level3"
          });
        }
        else {
          logEvent("failed", 3);
          timeRunning = false;
          user1 = 0;
          user2 = 0;
          user3 = 0;
          user4 = 0;

          dragable1a.x = MTLG.getOptions().width / 2 - 550;
          dragable1a.y = MTLG.getOptions().height / 2 - 400;
          dragable1b.x = MTLG.getOptions().width / 2 - 250;
          dragable1b.y = MTLG.getOptions().height / 2 - 400;
          dragable2a.x = MTLG.getOptions().width / 2 + 550;
          dragable2a.y = MTLG.getOptions().height / 2 - 400;
          dragable2b.x = MTLG.getOptions().width / 2 + 250;
          dragable2b.y = MTLG.getOptions().height / 2 - 400;
          dragable3a.x = MTLG.getOptions().width / 2 - 550;
          dragable3a.y = MTLG.getOptions().height / 2 + 400;
          dragable3b.x = MTLG.getOptions().width / 2 - 250;
          dragable3b.y = MTLG.getOptions().height / 2 + 400;
          dragable4a.x = MTLG.getOptions().width / 2 + 550;
          dragable4a.y = MTLG.getOptions().height / 2 + 400;
          dragable4b.x = MTLG.getOptions().width / 2 + 250;
          dragable4b.y = MTLG.getOptions().height / 2 + 400;

          stage.addChild(dragable1a);
          stage.addChild(dragable1b);
          stage.addChild(dragable2a);
          stage.addChild(dragable2b);
          stage.addChild(dragable3a);
          stage.addChild(dragable3b);
          stage.addChild(dragable4a);
          stage.addChild(dragable4b);
          stage.addChild(counter1);
          stage.addChild(counter2);
        }
      }
    }
  });


  //user 4 - first object
  var dragable4a = new createjs.Shape();
  dragable4a.graphics.beginFill(MTLG.getSettings().default.color_p4).drawRect(0, 0, 200, 200);
  dragable4a.regX = 200 / 2;
  dragable4a.regY = 200 / 2;
  dragable4a.rotation = 0;
  dragable4a.x = MTLG.getOptions().width / 2 + 550;
  dragable4a.y = MTLG.getOptions().height / 2 + 400;
  stage.addChild(dragable4a);

  dragable4a.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable4a.set(dragable4a.localToLocal(localX, localY, dragable4a.parent));
  });
  dragable4a.on("pressup", function (evt) {
    logEvent("dragged", 4);
    var distanceX = Math.abs(dragable4a.x - target2.x);
    var distanceY = Math.abs(dragable4a.y - target2.y);
    if ((distanceX <= 150) && (distanceY <= 50)) {
      logEvent("progressed", 4);
      stage.removeChild(dragable4a);
      user4++;
      if (!timeRunning) {
        stage.removeChild(counter1);
        stage.removeChild(counter2);
        timeRunning = true;
        timeStart = new Date();
      }
      if (user1 == 2 && user2 == 2 && user3 == 2 && user4 == 2) {
        timeEnd = new Date();
        if (((timeEnd - timeStart) / 1000) <= MTLG.getSettings().default.timeLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level3"
          });
        }
        else {
          logEvent("failed", 4);
          timeRunning = false;
          user1 = 0;
          user2 = 0;
          user3 = 0;
          user4 = 0;

          dragable1a.x = MTLG.getOptions().width / 2 - 550;
          dragable1a.y = MTLG.getOptions().height / 2 - 400;
          dragable1b.x = MTLG.getOptions().width / 2 - 250;
          dragable1b.y = MTLG.getOptions().height / 2 - 400;
          dragable2a.x = MTLG.getOptions().width / 2 + 550;
          dragable2a.y = MTLG.getOptions().height / 2 - 400;
          dragable2b.x = MTLG.getOptions().width / 2 + 250;
          dragable2b.y = MTLG.getOptions().height / 2 - 400;
          dragable3a.x = MTLG.getOptions().width / 2 - 550;
          dragable3a.y = MTLG.getOptions().height / 2 + 400;
          dragable3b.x = MTLG.getOptions().width / 2 - 250;
          dragable3b.y = MTLG.getOptions().height / 2 + 400;
          dragable4a.x = MTLG.getOptions().width / 2 + 550;
          dragable4a.y = MTLG.getOptions().height / 2 + 400;
          dragable4b.x = MTLG.getOptions().width / 2 + 250;
          dragable4b.y = MTLG.getOptions().height / 2 + 400;

          stage.addChild(dragable1a);
          stage.addChild(dragable1b);
          stage.addChild(dragable2a);
          stage.addChild(dragable2b);
          stage.addChild(dragable3a);
          stage.addChild(dragable3b);
          stage.addChild(dragable4a);
          stage.addChild(dragable4b);
          stage.addChild(counter1);
          stage.addChild(counter2);
        }
      }
    }
  });

  //user 4 - second object
  var dragable4b = new createjs.Shape();
  dragable4b.graphics.beginFill(MTLG.getSettings().default.color_p4).drawRect(0, 0, 200, 200);
  dragable4b.regX = 200 / 2;
  dragable4b.regY = 200 / 2;
  dragable4b.rotation = 0;
  dragable4b.x = MTLG.getOptions().width / 2 + 250;
  dragable4b.y = MTLG.getOptions().height / 2 + 400;
  stage.addChild(dragable4b);

  dragable4b.on('pressmove', function ({
    localX,
    localY
  }) {
    dragable4b.set(dragable4b.localToLocal(localX, localY, dragable4b.parent));
  });
  dragable4b.on("pressup", function (evt) {
    logEvent("dragged", 4);
    var distanceX = Math.abs(dragable4b.x - target1.x);
    var distanceY = Math.abs(dragable4b.y - target1.y);
    if ((distanceX <= 150) && (distanceY <= 50)) {
      logEvent("progressed", 4);
      stage.removeChild(dragable4b);
      user4++;
      if (!timeRunning) {
        stage.removeChild(counter1);
        stage.removeChild(counter2);
        timeRunning = true;
        timeStart = new Date();
      }
      if (user1 == 2 && user2 == 2 && user3 == 2 && user4 == 2) {
        timeEnd = new Date();
        if (((timeEnd - timeStart) / 1000) <= MTLG.getSettings().default.timeLimit) {
          logEvent("solved", 1);
          logEvent("solved", 2);
          logEvent("solved", 3);
          logEvent("solved", 4);
          MTLG.lc.levelFinished({
            nextLevel: "level3"
          });
        }
        else {
          logEvent("failed", 4);
          timeRunning = false;
          user1 = 0;
          user2 = 0;
          user3 = 0;
          user4 = 0;

          dragable1a.x = MTLG.getOptions().width / 2 - 550;
          dragable1a.y = MTLG.getOptions().height / 2 - 400;
          dragable1b.x = MTLG.getOptions().width / 2 - 250;
          dragable1b.y = MTLG.getOptions().height / 2 - 400;
          dragable2a.x = MTLG.getOptions().width / 2 + 550;
          dragable2a.y = MTLG.getOptions().height / 2 - 400;
          dragable2b.x = MTLG.getOptions().width / 2 + 250;
          dragable2b.y = MTLG.getOptions().height / 2 - 400;
          dragable3a.x = MTLG.getOptions().width / 2 - 550;
          dragable3a.y = MTLG.getOptions().height / 2 + 400;
          dragable3b.x = MTLG.getOptions().width / 2 - 250;
          dragable3b.y = MTLG.getOptions().height / 2 + 400;
          dragable4a.x = MTLG.getOptions().width / 2 + 550;
          dragable4a.y = MTLG.getOptions().height / 2 + 400;
          dragable4b.x = MTLG.getOptions().width / 2 + 250;
          dragable4b.y = MTLG.getOptions().height / 2 + 400;

          stage.addChild(dragable1a);
          stage.addChild(dragable1b);
          stage.addChild(dragable2a);
          stage.addChild(dragable2b);
          stage.addChild(dragable3a);
          stage.addChild(dragable3b);
          stage.addChild(dragable4a);
          stage.addChild(dragable4b);
          stage.addChild(counter1);
          stage.addChild(counter2);
        }
      }
    }
  });

}
