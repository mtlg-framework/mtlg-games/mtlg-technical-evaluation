
function thirdlevel_init() {
  console.log("These players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }

  console.log("These are the available game options:");
  console.log(MTLG.getOptions());

  console.log("These are the available game settings:");
  console.log(MTLG.getSettings());

  drawLevel3();
}

function checkLevel3(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "level3") {
    return 1;
  }
  return 0;
}


function drawLevel3() {
  var stage = MTLG.getStageContainer();

  console.log("Level 3 started.");

  var c;
  var ctx;
  var toDraw;
  var drawn;
  //var pressUp;
  var drawPhase = false;
  var error1 = 0;
  var error2 = 0;
  var newX;
  var newY;
  //var oldX;
  //var oldY;
  // var result;
  var i;
  //var finished = false;
  var standard = 414692; //for Microsoft Multitouch-Tablet (gets resetted for individual playground every round)
  gameOn = true;
  phaseOne = true;
  roundCounter = 1;

  var backgroundCanvas = new createjs.Shape();
  backgroundCanvas.graphics.beginFill('black').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
  backgroundCanvas.regX = 0;
  backgroundCanvas.regY = 0;
  backgroundCanvas.rotation = 0;
  backgroundCanvas.x = 0;
  backgroundCanvas.y = 0;
  stage.addChild(backgroundCanvas);

  //Skip level Button
  /*
  var skip = new createjs.Text("Skip", "bold 40px Lucida Console", MTLG.getSettings().default.main_color);
  skip.x = MTLG.getOptions().width / 2 - 675;
  skip.y = MTLG.getOptions().height / 2 + 340;
  skip.rotation = 90;
  stage.addChild(skip);

  skip.on("pressup", function (evt) {
    MTLG.lc.levelFinished({
      nextLevel: "pong"
    });
  });
  */

  //drawable object for user 1
  var object1a = new createjs.Shape();
  object1a.graphics.beginFill('white').drawRect(0, 0, 320, 30);
  object1a.regX = 0;
  object1a.regY = 0;
  object1a.rotation = 0;
  object1a.x = MTLG.getOptions().width / 2 - 600;
  object1a.y = MTLG.getOptions().height / 2 - 500;
  stage.addChild(object1a);

  var object1b = new createjs.Shape();
  object1b.graphics.beginFill('white').drawRect(0, 0, 30, 330);
  object1b.regX = 0;
  object1b.regY = 0;
  object1b.rotation = 0;
  object1b.x = MTLG.getOptions().width / 2 - 600 + 300;
  object1b.y = MTLG.getOptions().height / 2 - 500;
  stage.addChild(object1b);

  var object1c = new createjs.Shape();
  object1c.graphics.beginFill('white').drawRect(0, 0, 30, 330);
  object1c.regX = 0;
  object1c.regY = 0;
  object1c.rotation = 0;
  object1c.x = MTLG.getOptions().width / 2 - 600;
  object1c.y = MTLG.getOptions().height / 2 - 500;
  stage.addChild(object1c);

  var object1d = new createjs.Shape();
  object1d.graphics.beginFill('white').drawRect(0, 0, 320, 30);
  object1d.regX = 0;
  object1d.regY = 0;
  object1d.rotation = 0;
  object1d.x = MTLG.getOptions().width / 2 - 600;
  object1d.y = MTLG.getOptions().height / 2 - 500 + 300;
  stage.addChild(object1d);

  //drawable object for user 2
  var object2a = new createjs.Shape();
  object2a.graphics.beginFill('white').drawRect(0, 0, 320, 30);
  object2a.regX = 0;
  object2a.regY = 0;
  object2a.rotation = 0;
  object2a.x = MTLG.getOptions().width / 2 + 600 - 300;
  object2a.y = MTLG.getOptions().height / 2 - 500;
  stage.addChild(object2a);

  var object2b = new createjs.Shape();
  object2b.graphics.beginFill('white').drawRect(0, 0, 30, 330);
  object2b.regX = 0;
  object2b.regY = 0;
  object2b.rotation = 0;
  object2b.x = MTLG.getOptions().width / 2 + 600 - 300;
  object2b.y = MTLG.getOptions().height / 2 - 500;
  stage.addChild(object2b);

  var object2c = new createjs.Shape();
  object2c.graphics.beginFill('white').drawRect(0, 0, 30, 330);
  object2c.regX = 0;
  object2c.regY = 0;
  object2c.rotation = 0;
  object2c.x = MTLG.getOptions().width / 2 + 600;
  object2c.y = MTLG.getOptions().height / 2 - 500;
  stage.addChild(object2c);

  var object2d = new createjs.Shape();
  object2d.graphics.beginFill('white').drawRect(0, 0, 320, 30);
  object2d.regX = 0;
  object2d.regY = 0;
  object2d.rotation = 0;
  object2d.x = MTLG.getOptions().width / 2 + 600 - 300;
  object2d.y = MTLG.getOptions().height / 2 - 500 + 300;
  stage.addChild(object2d);

  //drawable object for user 3
  var object3a = new createjs.Shape();
  object3a.graphics.beginFill('white').drawRect(0, 0, 320, 30);
  object3a.regX = 0;
  object3a.regY = 0;
  object3a.rotation = 0;
  object3a.x = MTLG.getOptions().width / 2 - 600;
  object3a.y = MTLG.getOptions().height / 2 + 500;
  stage.addChild(object3a);

  var object3b = new createjs.Shape();
  object3b.graphics.beginFill('white').drawRect(0, 0, 30, 330);
  object3b.regX = 0;
  object3b.regY = 0;
  object3b.rotation = 0;
  object3b.x = MTLG.getOptions().width / 2 - 600 + 300;
  object3b.y = MTLG.getOptions().height / 2 + 500 - 300;
  stage.addChild(object3b);

  var object3c = new createjs.Shape();
  object3c.graphics.beginFill('white').drawRect(0, 0, 30, 330);
  object3c.regX = 0;
  object3c.regY = 0;
  object3c.rotation = 0;
  object3c.x = MTLG.getOptions().width / 2 - 600;
  object3c.y = MTLG.getOptions().height / 2 + 500 - 300;
  stage.addChild(object3c);

  var object3d = new createjs.Shape();
  object3d.graphics.beginFill('white').drawRect(0, 0, 320, 30);
  object3d.regX = 0;
  object3d.regY = 0;
  object3d.rotation = 0;
  object3d.x = MTLG.getOptions().width / 2 - 600;
  object3d.y = MTLG.getOptions().height / 2 + 500 - 300;
  stage.addChild(object3d);

  //drawable object for user 4
  var object4a = new createjs.Shape();
  object4a.graphics.beginFill('white').drawRect(0, 0, 320, 30);
  object4a.regX = 0;
  object4a.regY = 0;
  object4a.rotation = 0;
  object4a.x = MTLG.getOptions().width / 2 + 600 - 300;
  object4a.y = MTLG.getOptions().height / 2 + 500 - 300;
  stage.addChild(object4a);

  var object4b = new createjs.Shape();
  object4b.graphics.beginFill('white').drawRect(0, 0, 30, 330);
  object4b.regX = 0;
  object4b.regY = 0;
  object4b.rotation = 0;
  object4b.x = MTLG.getOptions().width / 2 + 600 - 300;
  object4b.y = MTLG.getOptions().height / 2 + 500 - 300;
  stage.addChild(object4b);

  var object4c = new createjs.Shape();
  object4c.graphics.beginFill('white').drawRect(0, 0, 30, 330);
  object4c.regX = 0;
  object4c.regY = 0;
  object4c.rotation = 0;
  object4c.x = MTLG.getOptions().width / 2 + 600;
  object4c.y = MTLG.getOptions().height / 2 + 500 - 300;
  stage.addChild(object4c);

  var object4d = new createjs.Shape();
  object4d.graphics.beginFill('white').drawRect(0, 0, 320, 30);
  object4d.regX = 0;
  object4d.regY = 0;
  object4d.rotation = 0;
  object4d.x = MTLG.getOptions().width / 2 + 600 - 300;
  object4d.y = MTLG.getOptions().height / 2 + 500;
  stage.addChild(object4d);

  //Enables the user to draw on the canvas

  var pidX = new Map();
  var pidY = new Map();
  var pidPressUp = new Map();

  backgroundCanvas.on("pressmove", function (evt) {
    //console.log(evt);
    if (drawPhase && !(pidPressUp.get(evt.pointerID) == undefined) && pidPressUp.get(evt.pointerID) == 0) {
      newX = evt.localX;
      newY = evt.localY;
      backgroundCanvas.graphics.setStrokeStyle(10);
      //Error-Marge hängt von Farbe ab? But why?
      /*
      if (evt.localX <= (MTLG.getOptions().width / 2) && evt.localY <= (MTLG.getOptions().height / 2)) {
        backgroundCanvas.graphics.beginStroke('white');
      }
      else if (evt.localX > (MTLG.getOptions().width / 2) && evt.localY <= (MTLG.getOptions().height / 2)) {
        backgroundCanvas.graphics.beginStroke('white');
      }
      else if (evt.localX <= (MTLG.getOptions().width / 2) && evt.localY > (MTLG.getOptions().height / 2)) {
        backgroundCanvas.graphics.beginStroke('white');
      }
      else if (evt.localX > (MTLG.getOptions().width / 2) && evt.localY > (MTLG.getOptions().height / 2)) {
        backgroundCanvas.graphics.beginStroke('white');
      }
      */
      backgroundCanvas.graphics.beginStroke('white');
      backgroundCanvas.graphics.moveTo(pidX.get(evt.pointerID), pidY.get(evt.pointerID));
      backgroundCanvas.graphics.lineTo(newX, newY)
      backgroundCanvas.graphics.endStroke();
      pidX.set(evt.pointerID, newX);
      pidY.set(evt.pointerID, newY);
    }
    else if (drawPhase) {
      pidPressUp.set(evt.pointerID, 0);
      pidX.set(evt.pointerID, evt.localX);
      pidY.set(evt.pointerID, evt.localY);
    }
  });

  backgroundCanvas.on("pressup", function (evt) {
    pidPressUp.set(evt.pointerID, 1);
    /*
    console.log(evt.localX);
    console.log(evt.localX);
    console.log(MTLG.getOptions().width / 2);
    console.log(MTLG.getOptions().height / 2);
    */
    if (evt.localX <= (MTLG.getOptions().width / 2) && evt.localY <= (MTLG.getOptions().height / 2)) {
      logEvent("drew", 1);
    }
    else if (evt.localX > (MTLG.getOptions().width / 2) && evt.localY <= (MTLG.getOptions().height / 2)) {
      logEvent("drew", 2);
    }
    else if (evt.localX <= (MTLG.getOptions().width / 2) && evt.localY > (MTLG.getOptions().height / 2)) {
      logEvent("drew", 3);
    }
    else if (evt.localX > (MTLG.getOptions().width / 2) && evt.localY > (MTLG.getOptions().height / 2)) {
      logEvent("drew", 4);
    }
  });

  /*
    //Try 1, phase 1
    setTimeout(
      function () {
        if (!finished) {
          console.log("Phase 1");
          backgroundCanvas.graphics.beginFill('black').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          c = document.getElementById("canvasObject");
          ctx = c.getContext("2d");
          toDraw = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          stage.removeChild(object1a);
          stage.removeChild(object1b);
          stage.removeChild(object1c);
          stage.removeChild(object1d);
          stage.removeChild(object2a);
          stage.removeChild(object2b);
          stage.removeChild(object2c);
          stage.removeChild(object2d);
          stage.removeChild(object3a);
          stage.removeChild(object3b);
          stage.removeChild(object3c);
          stage.removeChild(object3d);
          stage.removeChild(object4a);
          stage.removeChild(object4b);
          stage.removeChild(object4c);
          stage.removeChild(object4d);
          drawPhase = true;
        }
      }, (1 * MTLG.getSettings().default.drawingTime));

    setTimeout(
      function () {
        if (!finished) {
          c = document.getElementById("canvasObject");
          ctx = c.getContext("2d");
          drawn = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          error1 = 0;
          error2 = 0;
          for (i = 0; i < toDraw.data.length; i++) {
            if (toDraw.data[i] > drawn.data[i]) {
              error1++;
            }
            else if (toDraw.data[i] < drawn.data[i]) {
              error2++;
            }
          }
          result = error1 + Math.floor(error2 / 10);
          standard = result;
          console.log("Standard got set.");
          console.log(standard);
        }
      }, (1 * MTLG.getSettings().default.drawingTime + 50));

    //try 1, phase 2
    setTimeout(
      function () {
        if (!finished) {
          console.log("Phase 2");
          drawPhase = false;
          c = document.getElementById("canvasObject");
          ctx = c.getContext("2d");
          drawn = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          //console.log(toDraw.data);
          //console.log(drawn.data);
          error1 = 0;
          error2 = 0;
          for (i = 0; i < toDraw.data.length; i++) {
            if (toDraw.data[i] > drawn.data[i]) {
              error1++;
            }
            else if (toDraw.data[i] < drawn.data[i]) {
              error2++;
            }
          }
          result = error1 + Math.floor(error2 / 10);
          //console.log(result);
          evaluateScore(result, 1);
          stage.addChild(object1a);
          stage.addChild(object1b);
          stage.addChild(object1c);
          stage.addChild(object1d);
          stage.addChild(object2a);
          stage.addChild(object2b);
          stage.addChild(object2c);
          stage.addChild(object2d);
          stage.addChild(object3a);
          stage.addChild(object3b);
          stage.addChild(object3c);
          stage.addChild(object3d);
          stage.addChild(object4a);
          stage.addChild(object4b);
          stage.addChild(object4c);
          stage.addChild(object4d);
        }
      }, (2 * MTLG.getSettings().default.drawingTime));

    //try 2, phase 1
    setTimeout(
      function () {
        if (!finished) {
          console.log("Phase 3");
          c = document.getElementById("canvasObject");
          ctx = c.getContext("2d");
          //toDraw = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          stage.removeChild(object1a);
          stage.removeChild(object1b);
          stage.removeChild(object1c);
          stage.removeChild(object1d);
          stage.removeChild(object2a);
          stage.removeChild(object2b);
          stage.removeChild(object2c);
          stage.removeChild(object2d);
          stage.removeChild(object3a);
          stage.removeChild(object3b);
          stage.removeChild(object3c);
          stage.removeChild(object3d);
          stage.removeChild(object4a);
          stage.removeChild(object4b);
          stage.removeChild(object4c);
          stage.removeChild(object4d);
          backgroundCanvas.graphics.beginFill('black').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          drawPhase = true;
        }
      }, (3 * MTLG.getSettings().default.drawingTime));

    //try 2, phase 2
    setTimeout(
      function () {
        if (!finished) {
          console.log("Phase 4");
          drawPhase = false;
          c = document.getElementById("canvasObject");
          ctx = c.getContext("2d");
          drawn = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          //console.log(toDraw.data);
          //console.log(drawn.data);
          error1 = 0;
          error2 = 0;
          for (i = 0; i < toDraw.data.length; i++) {
            if (toDraw.data[i] > drawn.data[i]) {
              error1++;
            }
            else if (toDraw.data[i] < drawn.data[i]) {
              error2++;
            }
          }
          result = error1 + Math.floor(error2 / 10);
          //console.log(result);
          evaluateScore(result, 2);
          stage.addChild(object1a);
          stage.addChild(object1b);
          stage.addChild(object1c);
          stage.addChild(object1d);
          stage.addChild(object2a);
          stage.addChild(object2b);
          stage.addChild(object2c);
          stage.addChild(object2d);
          stage.addChild(object3a);
          stage.addChild(object3b);
          stage.addChild(object3c);
          stage.addChild(object3d);
          stage.addChild(object4a);
          stage.addChild(object4b);
          stage.addChild(object4c);
          stage.addChild(object4d);
          //backgroundCanvas.graphics.beginFill('black').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          //drawPhase = true;
        }
      }, (4 * MTLG.getSettings().default.drawingTime));

    //try 3, phase 1
    setTimeout(
      function () {
        if (!finished) {
          console.log("Phase 5");
          c = document.getElementById("canvasObject");
          ctx = c.getContext("2d");
          //toDraw = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          stage.removeChild(object1a);
          stage.removeChild(object1b);
          stage.removeChild(object1c);
          stage.removeChild(object1d);
          stage.removeChild(object2a);
          stage.removeChild(object2b);
          stage.removeChild(object2c);
          stage.removeChild(object2d);
          stage.removeChild(object3a);
          stage.removeChild(object3b);
          stage.removeChild(object3c);
          stage.removeChild(object3d);
          stage.removeChild(object4a);
          stage.removeChild(object4b);
          stage.removeChild(object4c);
          stage.removeChild(object4d);
          backgroundCanvas.graphics.beginFill('black').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          drawPhase = true;
        }
      }, (5 * MTLG.getSettings().default.drawingTime));

    //try 3, phase 2
    setTimeout(
      function () {
        if (!finished) {
          console.log("Phase 6");
          drawPhase = false;
          c = document.getElementById("canvasObject");
          ctx = c.getContext("2d");
          drawn = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          //console.log(toDraw.data);
          //console.log(drawn.data);
          error1 = 0;
          error2 = 0;
          for (i = 0; i < toDraw.data.length; i++) {
            if (toDraw.data[i] > drawn.data[i]) {
              error1++;
            }
            else if (toDraw.data[i] < drawn.data[i]) {
              error2++;
            }
          }
          result = error1 + Math.floor(error2 / 10);
          //console.log(result);
          evaluateScore(result, 3);
          stage.addChild(object1a);
          stage.addChild(object1b);
          stage.addChild(object1c);
          stage.addChild(object1d);
          stage.addChild(object2a);
          stage.addChild(object2b);
          stage.addChild(object2c);
          stage.addChild(object2d);
          stage.addChild(object3a);
          stage.addChild(object3b);
          stage.addChild(object3c);
          stage.addChild(object3d);
          stage.addChild(object4a);
          stage.addChild(object4b);
          stage.addChild(object4c);
          stage.addChild(object4d);
          //backgroundCanvas.graphics.beginFill('black').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
        }
      }, (6 * MTLG.getSettings().default.drawingTime));

    setTimeout(
      function () {
        MTLG.lc.levelFinished({
          nextLevel: "pong"
        });
      }, (7 * MTLG.getSettings().default.drawingTime));
  */

  backgroundCanvas.graphics.beginFill('black').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
  c = document.getElementById("canvasObject");
  ctx = c.getContext("2d");
  toDraw = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);

  //Set standard
  setTimeout(
    function () {
      c = document.getElementById("canvasObject");
      ctx = c.getContext("2d");
      drawn = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
      error1 = 0;
      error2 = 0;
      for (i = 0; i < toDraw.data.length; i++) {
        if (toDraw.data[i] > drawn.data[i]) {
          error1++;
        }
        else if (toDraw.data[i] < drawn.data[i]) {
          error2++;
        }
      }
      result = error1 + Math.floor(error2 / 10);
      standard = result;
      console.log("Standard got set.");
      console.log(standard);
    }, 200);

  //Endless iterations if necessary
  function startGame() {
    if (gameOn) {
      setTimeout(function () {
        if (phaseOne) {
          c = document.getElementById("canvasObject");
          ctx = c.getContext("2d");
          stage.removeChild(object1a);
          stage.removeChild(object1b);
          stage.removeChild(object1c);
          stage.removeChild(object1d);
          stage.removeChild(object2a);
          stage.removeChild(object2b);
          stage.removeChild(object2c);
          stage.removeChild(object2d);
          stage.removeChild(object3a);
          stage.removeChild(object3b);
          stage.removeChild(object3c);
          stage.removeChild(object3d);
          stage.removeChild(object4a);
          stage.removeChild(object4b);
          stage.removeChild(object4c);
          stage.removeChild(object4d);
          backgroundCanvas.graphics.beginFill('black').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          drawPhase = true;
          phaseOne = false;
        }
        else {
          phaseOne = true;
          drawPhase = false;
          roundCounter = roundCounter + 1;
          c = document.getElementById("canvasObject");
          ctx = c.getContext("2d");
          drawn = ctx.getImageData(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
          error1 = 0;
          error2 = 0;
          for (i = 0; i < toDraw.data.length; i++) {
            if (toDraw.data[i] > drawn.data[i]) {
              error1++;
            }
            else if (toDraw.data[i] < drawn.data[i]) {
              error2++;
            }
          }
          result = error1 + Math.floor(error2 / 10);
          evaluateScore(result, roundCounter);
          stage.addChild(object1a);
          stage.addChild(object1b);
          stage.addChild(object1c);
          stage.addChild(object1d);
          stage.addChild(object2a);
          stage.addChild(object2b);
          stage.addChild(object2c);
          stage.addChild(object2d);
          stage.addChild(object3a);
          stage.addChild(object3b);
          stage.addChild(object3c);
          stage.addChild(object3d);
          stage.addChild(object4a);
          stage.addChild(object4b);
          stage.addChild(object4c);
          stage.addChild(object4d);
        }

        startGame();
      }, MTLG.getSettings().default.drawingTime);
    }
  }

  startGame();

  function evaluateScore(score, round) {
    //console.log(score)
    scoreBias = score;
    
    /*
    first = true;
    if (first) {
      first = false;
      var _newDefaultSettings = {
        default: {
          win_limit: 1
        }
      };
      MTLG.loadSettings(_newDefaultSettings);
    }
    */
    logEvent("answered", 1);
    logEvent("answered", 2);
    logEvent("answered", 3);
    logEvent("answered", 4);

    //Use round bias
    scoreBias = Math.floor(score * (1 - (round * 0.01)));
    console.log("Round bias has been calculated.")
    console.log(scoreBias)

    if (scoreBias <= (0.80 * standard)) {
      score1.text = "10/10";
      score2.text = "10/10";
      score1.color = 'green';
      score2.color = 'green';

      gameOn = false;
      setTimeout(
        function () {
          MTLG.lc.levelFinished({
            nextLevel: "pong"
          });
        }, (MTLG.getSettings().default.drawingTime / 2));
    }
    else if (scoreBias <= (0.82 * standard)) {
      score1.text = "9/10";
      score2.text = "9/10";
      score1.color = 'green';
      score2.color = 'green';

      gameOn = false;
      setTimeout(
        function () {
          MTLG.lc.levelFinished({
            nextLevel: "pong"
          });
        }, (MTLG.getSettings().default.drawingTime / 2));
    }
    else if (scoreBias <= (0.84 * standard)) {
      score1.text = "8/10";
      score2.text = "8/10";
      score1.color = 'green';
      score2.color = 'green';

      gameOn = false;
      setTimeout(
        function () {
          MTLG.lc.levelFinished({
            nextLevel: "pong"
          });
        }, (MTLG.getSettings().default.drawingTime / 2));
    }
    else if (scoreBias <= (0.86 * standard)) {
      score1.text = "7/10";
      score2.text = "7/10";
      score1.color = 'red';
      score2.color = 'red';
    }
    else if (scoreBias <= (0.88 * standard)) {
      score1.text = "6/10";
      score2.text = "6/10";
      score1.color = 'red';
      score2.color = 'red';
    }
    else if (scoreBias <= (0.90 * standard)) {
      score1.text = "5/10";
      score2.text = "5/10";
      score1.color = 'red';
      score2.color = 'red';
    }
    else if (scoreBias <= (0.92 * standard)) {
      score1.text = "4/10";
      score2.text = "4/10";
      score1.color = 'red';
      score2.color = 'red';
    }
    else if (scoreBias <= (0.94 * standard)) {
      score1.text = "3/10";
      score2.text = "3/10";
      score1.color = 'red';
      score2.color = 'red';
    }
    else if (scoreBias <= (0.96 * standard)) {
      score1.text = "2/10";
      score2.text = "2/10";
      score1.color = 'red';
      score2.color = 'red';
    }
    else if (scoreBias <= (0.98 * standard)) {
      score1.text = "1/10";
      score2.text = "1/10";
      score1.color = 'red';
      score2.color = 'red';
    }
    else {
      score1.text = "0/10";
      score2.text = "0/10";
      score1.color = 'red';
      score2.color = 'red';
    }
  }

  var score1 = new createjs.Text("0/10", "bold 40px Lucida Console", MTLG.getSettings().default.main_color);
  score1.x = MTLG.getOptions().width / 2 + 55;
  score1.y = MTLG.getOptions().height / 2 - 360;
  score1.rotation = 180;
  stage.addChild(score1);

  var score2 = new createjs.Text("0/10", "bold 40px Lucida Console", MTLG.getSettings().default.main_color);
  score2.x = MTLG.getOptions().width / 2 - 55;
  score2.y = MTLG.getOptions().height / 2 + 360;
  score2.rotation = 0;
  stage.addChild(score2);

}
