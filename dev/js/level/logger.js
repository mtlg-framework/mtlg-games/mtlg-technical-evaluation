var index = 0;

function logEvent(action, player) {
  //log to local storage

  if (MTLG.getPlayer(0) && MTLG.getPlayer(0).position == player) {
    player = MTLG.getPlayer(0).pseudonym;
  }
  else if (MTLG.getPlayer(1) && MTLG.getPlayer(1).position == player) {
    player = MTLG.getPlayer(1).pseudonym;
  }
  else if (MTLG.getPlayer(2) && MTLG.getPlayer(2).position == player) {
    player = MTLG.getPlayer(2).pseudonym;
  }
  else if (MTLG.getPlayer(3) && MTLG.getPlayer(3).position == player) {
    player = MTLG.getPlayer(3).pseudonym;
  }
  else {
    player = "Player " + player;
  }

  var timestamp = new Date() + "";
  timestamp = timestamp.substring(0, 33);
  var localLog = "" + player + " " + action + ". --- " + "Timestamp: " + timestamp + "\n";
  localStorage.setItem(index.toString(), localLog);
  index++;

  //log to learning locker
  //"http://xapi.elearn.rwth-aachen.de/definitions/multitouch/verbs/dragged"
  //"http://xapi.elearn.rwth-aachen.de/definitions/multitouch/verbs/clicked"
  let stmt = {
    actor: {
      objectType: "Agent",
      id: "mailto:agent@mtlg.com",
      name: "" + player,
    },
    verb: {
      id: "http://xapi.elearn.rwth-aachen.de/definitions/multitouch/verbs/" + action,
      display: {
        en: action,
      }
    },
    object: {
      id: "http://sendActivityStatementFunction",
    }
  };

  //send to learning locker
  MTLG.xapidatacollector.sendStatement(stmt).then(function (stmtID) {
    console.log(stmtID);
  }).catch(function (error) {
    console.log(error.message);
  });

}

function downloadLogfile() {
  //download logged content from local storage
  var text = "";

  for (var i = 0; i < index; i++) {
    text = text.concat(localStorage.getItem(i.toString()));
  }

  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', 'gamelog.txt');

  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);

  //In addition: Push logfile to CKAN instance
  //newDataset(text);
}
