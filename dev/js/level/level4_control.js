var parse_query_string = function (query) {
  var vars = query.split("&");
  var query_string = {};
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    var key = decodeURIComponent(pair[0]);
    var value = decodeURIComponent(pair[1]);
    // If first entry with this name
    if (typeof query_string[key] === "undefined") {
      query_string[key] = decodeURIComponent(value);
    }
  }
  return query_string;
}


function control_init() {
  var room_name = null;
  if (window.location.search.indexOf('room') !== -1) {
    var params = parse_query_string(window.location.search.substring(1));
    room_name = params['room'];
  }
  console.log(room_name);
  MTLG.distributedDisplays.rooms.createRoom(room_name || 'pong', function (result) {
    if (result && result.success) {
      console.error("You have to create the master pong room first.");
    } else {
      MTLG.distributedDisplays.rooms.joinRoom(room_name || 'pong', function (result) {
        if (result && result.success) {
          console.log("Joined room.")
          afterJoining(room_name);
        } else {
          console.error("Something went wrong in creating or joining the room for pong");
        }
      })
    }
  });
}

function afterJoining(room_name) {
  MTLG.distributedDisplays.actions.setCustomFunction('receivePlayers', function (players) {

    console.log('players', players);
    if (players[0] == null) {
      console.error("Nobody logged in to play.")
    }
    else {
      drawPlayerSelection(players, room_name);
    }
  });

  MTLG.distributedDisplays.communication.sendCustomAction(room_name || 'pong', 'getPlayers', MTLG.distributedDisplays.rooms.getClientId()); //Erfrage alle Spielerdetails
}

function checkControl(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel === "control") {
    return 1;
  }
  return 0;
}


function drawPlayerSelection(players, room_name) {
  players.forEach((player, index) => {
    let playerButton = MTLG.utils.uiElements.addButton({ text: player.pseudonym, sizeX: MTLG.getOptions().width * 8 / 10, sizeY: MTLG.getOptions().height / 5 }, () => {
      MTLG.getStageContainer().removeAllChildren();
      drawControl(player, room_name);
    });
    playerButton.x = MTLG.getOptions().width / 2;
    playerButton.y = MTLG.getOptions().height / 5 + index * (MTLG.getOptions().height / 5 + 10);
    MTLG.getStageContainer().addChild(playerButton);
  })

}

function drawControl(player, room_name) {
  MTLG.distributedDisplays.communication.sendCustomAction(room_name || 'pong', "player_selected", true) //Gebe Bescheid, dass Spieler X angemeldet ist

  console.log("Slave created")
  var stage = MTLG.getStageContainer();

  var buttonLeft = MTLG.utils.uiElements.addButton({ text: "▼", sizeX: MTLG.getOptions().width, sizeY: MTLG.getOptions().height }, () => {
    // send control command to main
    MTLG.distributedDisplays.communication.sendCustomAction(room_name || "pong", "moveBar", { //ISSUE SOLVED
      control: "move",
      player: player
    })
    console.log("MOVE BAR command has been sent.");
  });
  buttonLeft.y = MTLG.getOptions().height / 2;
  buttonLeft.x = MTLG.getOptions().width / 2;
  stage.addChild(buttonLeft);

  /*
  var buttonRight = MTLG.utils.uiElements.addButton({ text: "DOWN", sizeX: MTLG.getOptions().width, sizeY: MTLG.getOptions().height / 2 }, () => {
    // send control command to main
    MTLG.distributedDisplays.communication.sendCustomAction(room_name || "pong", "moveBar", { //ISSUE SOLVED
      control: "down",
      player: player
    })
    console.log("DOWN command has been sent.");
  });
  buttonRight.y = MTLG.getOptions().height / 4 * 3;
  buttonRight.x = MTLG.getOptions().width / 2;
  stage.addChild(buttonRight);
  */

}
