

function fourthlevel_init() {
  console.log("These players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }

  console.log("These are the available game options:");
  console.log(MTLG.getOptions());

  console.log("These are the available game settings:");
  console.log(MTLG.getSettings());

  drawMenu();
}


function checkLevel4(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel === "level4") {
    return 1;
  }
  return 0;
}

function drawMenu() {
  var stage = MTLG.getStageContainer();

  let levelButton = MTLG.utils.uiElements.addButton({
    text: MTLG.lang.getString("Pong"),
    sizeX: MTLG.getOptions().width * 0.1,
    sizeY: 70,
  },
    function () {
      MTLG.lc.levelFinished({
        nextLevel: "pong"
      });
    }
  );
  levelButton.x = MTLG.getOptions().width * 1 / 3;
  levelButton.y = MTLG.getOptions().height / 2;

  let levelButton2 = MTLG.utils.uiElements.addButton({
    text: MTLG.lang.getString("Steuerung"),
    sizeX: MTLG.getOptions().width * 0.1,
    sizeY: 70,
  },
    function () {
      MTLG.lc.levelFinished({
        nextLevel: "control"
      });
    }
  );
  levelButton2.x = MTLG.getOptions().width * 2 / 3;
  levelButton2.y = MTLG.getOptions().height / 2;

  // add objects to stage
  stage.addChild(levelButton);
  stage.addChild(levelButton2);

}
