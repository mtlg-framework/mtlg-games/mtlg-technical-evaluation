

function ckan_init() {
  console.log("These are the available game options:");
  console.log(MTLG.getOptions());

  console.log("These are the available game settings:");
  console.log(MTLG.getSettings());

  newDataset('CKAN-Test');
}

function checkCKAN(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "ckan") {
    return 1;
  }
  return 0;
}

function newDataset(text) {
  /*
  var url = ['http://opendata.elearn.rwth-aachen.de/api/action/tag_list', 'http://opendata.elearn.rwth-aachen.de/api/action/group_list', 'http://opendata.elearn.rwth-aachen.de/api/action/package_list'];
  let answer = axios.get(url)
    .then(function (response) {
      console.log("Successful CKAN API request!")
      console.log(response);
      console.log(answer);
    })
    .catch(function (error) {
      console.log(error);
    });
  */

  //Using the CKAN library
  //var CKAN = require('ckan');

  var client = new CKAN.Client('https://opendata.elearn.rwth-aachen.de/', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzQxNjE4MDYsImp0aSI6IkZzWHNHc2xVNDh5cmRzcEJZM3h2Z2d6bll1Y0FKb1JHNG5IelM4RzZGMWxsbUotZlBWSXNSVzRVZTZBWHB3TmxmTDNlQUU3bUFlN3FWMV9PIn0.Eldtn0vn7hDGln4nBGGGfkMZQ4fAbpYFb5Ig3XSJH0o');
  client.requestType = 'GET';

  client.action('dataset_create', { name: text }, function (err, result) {
    console.log(err);
    console.log(result);
  })
}

/*
#!/usr/bin/env python
import urllib2
import urllib
import json
import pprint

# Put the details of the dataset we're going to create into a dict.
dataset_dict = {
    'name': 'my_dataset_name',
    'notes': 'A long description of my dataset',
    'owner_org': 'org_id_or_name'
}

# Use the json module to dump the dictionary to a string for posting.
data_string = urllib.quote(json.dumps(dataset_dict))

# We'll use the package_create function to create a new dataset.
request = urllib2.Request(
    'http://www.my_ckan_site.com/api/action/package_create')

# Creating a dataset requires an authorization header.
# Replace *** with your API key, from your user account on the CKAN site
# that you're creating the dataset on.
request.add_header('Authorization', '***')

# Make the HTTP request.
response = urllib2.urlopen(request, data_string)
assert response.code == 200

# Use the json module to load CKAN's response into a dictionary.
response_dict = json.loads(response.read())
assert response_dict['success'] is True

# package_create returns the created package as its result.
created_package = response_dict['result']
pprint.pprint(created_package)
*/
