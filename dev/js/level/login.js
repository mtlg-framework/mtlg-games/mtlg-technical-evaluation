

function login_init() {
  console.log("These are the available game options:");
  console.log(MTLG.getOptions());

  console.log("These are the available game settings:");
  console.log(MTLG.getSettings());

  drawLogin();
}

function checkLogin(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "login") {
    return 1;
  }
  return 0;
}


async function drawLogin() {
  var stage = MTLG.getStageContainer();
  console.log("Login process started.");

  MTLG.login.showLogin();

  //Check if all players are ready to begin

  var player1 = false;
  var player2 = false;
  var player3 = false;
  var player4 = false;

  var readyButton1 = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  readyButton1.regX = 100 / 2;
  readyButton1.regY = 100 / 2;
  readyButton1.rotation = 180;
  readyButton1.x = MTLG.getOptions().width / 2 - 210;
  readyButton1.y = MTLG.getOptions().height / 2 - 50;
  stage.addChild(readyButton1);

  readyButton1.on("pressup", function (evt) {
    readyButton1.text = "";
    player1 = true;
    logEvent("voted", 1)
    if (player1 && player2 && player3 && player4) {
      MTLG.login.hideLogin();
      logEvent("triggered", 1);
      stage.removeChild(readyButton1);
      stage.removeChild(readyButton2);
      stage.removeChild(readyButton3);
      stage.removeChild(readyButton4);
      surveyPhase();
    }
  });


  var readyButton2 = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  readyButton2.regX = 100 / 2;
  readyButton2.regY = 100 / 2;
  readyButton2.rotation = 180;
  readyButton2.x = MTLG.getOptions().width / 2 + 750;
  readyButton2.y = MTLG.getOptions().height / 2 - 50;
  stage.addChild(readyButton2);

  readyButton2.on("pressup", function (evt) {
    readyButton2.text = "";
    player2 = true;
    logEvent("voted", 2)
    if (player1 && player2 && player3 && player4) {
      MTLG.login.hideLogin();
      logEvent("triggered", 2);
      stage.removeChild(readyButton1);
      stage.removeChild(readyButton2);
      stage.removeChild(readyButton3);
      stage.removeChild(readyButton4);
      surveyPhase();
    }
  });


  var readyButton3 = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  readyButton3.regX = 100 / 2;
  readyButton3.regY = 100 / 2;
  readyButton3.rotation = 0;
  readyButton3.x = MTLG.getOptions().width / 2 - 750;
  readyButton3.y = MTLG.getOptions().height / 2 + 40;
  stage.addChild(readyButton3);

  readyButton3.on("pressup", function (evt) {
    readyButton3.text = "";
    player3 = true;
    logEvent("voted", 3)
    if (player1 && player2 && player3 && player4) {
      MTLG.login.hideLogin();
      logEvent("triggered", 3);
      stage.removeChild(readyButton1);
      stage.removeChild(readyButton2);
      stage.removeChild(readyButton3);
      stage.removeChild(readyButton4);
      surveyPhase();
    }
  });


  var readyButton4 = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  readyButton4.regX = 100 / 2;
  readyButton4.regY = 100 / 2;
  readyButton4.rotation = 0;
  readyButton4.x = MTLG.getOptions().width / 2 + 210;
  readyButton4.y = MTLG.getOptions().height / 2 + 40;
  stage.addChild(readyButton4);

  readyButton4.on("pressup", function (evt) {
    readyButton4.text = "";
    player4 = true;
    logEvent("voted", 4)
    if (player1 && player2 && player3 && player4) {
      MTLG.login.hideLogin();
      logEvent("triggered", 4);
      stage.removeChild(readyButton1);
      stage.removeChild(readyButton2);
      stage.removeChild(readyButton3);
      stage.removeChild(readyButton4);
      surveyPhase();
    }
  });

}

function create_qr_link(pseudonym) {
  const init = {
    method: 'GET',
    mode: 'no-cors',
  }
  const url0 = 'https://mtlg.elearn.rwth-aachen.de/ls_connector/big5/link/' + pseudonym //player.pseudonym;
  //const request0 = new Request(url0, init);

  return axios.get(url0)
    .then(function (response) {
      return response.data
    })
    .catch(function (error) {
      console.log(error);
      return "Error"
    });

}

async function surveyPhase(pseudonym) {
  var stage = MTLG.getStageContainer();

  var x = [-300, 650, -650, 300];
  var y = [-100, -100, 100, 100];
  var rotation = [180, 180, 0, 0];

  //console.log(create_qr_link(pseudonym))
  if (!MTLG.getPlayer(0)) {
    MTLG.lc.levelFinished({
      nextLevel: "level1"
    });
  }
  else if (MTLG.getPlayer(0)) {
    MTLG.utils.qrcode.createQRCode(await create_qr_link(MTLG.getPlayer(0).pseudonym), (obj) => {
      obj.bitmap.x = MTLG.getOptions().width / 2 + x[MTLG.getPlayer(0).position - 1];
      obj.bitmap.y = MTLG.getOptions().height / 2 + y[MTLG.getPlayer(0).position - 1];
      obj.bitmap.rotation = rotation[MTLG.getPlayer(0).position - 1];
      MTLG.getStageContainer().addChild(obj.bitmap)
    }, { width: MTLG.getOptions().height / 3, showText: false, fontColor: 'white' })
  }

  if (MTLG.getPlayer(1)) {
    MTLG.utils.qrcode.createQRCode(await create_qr_link(MTLG.getPlayer(1).pseudonym), (obj) => {
      obj.bitmap.x = MTLG.getOptions().width / 2 + x[MTLG.getPlayer(1).position - 1];
      obj.bitmap.y = MTLG.getOptions().height / 2 + y[MTLG.getPlayer(1).position - 1];
      obj.bitmap.rotation = rotation[MTLG.getPlayer(1).position - 1];
      MTLG.getStageContainer().addChild(obj.bitmap)
    }, { width: MTLG.getOptions().height / 3, showText: false, fontColor: 'white' })
  }

  if (MTLG.getPlayer(2)) {
    MTLG.utils.qrcode.createQRCode(await create_qr_link(MTLG.getPlayer(2).pseudonym), (obj) => {
      obj.bitmap.x = MTLG.getOptions().width / 2 + x[MTLG.getPlayer(2).position - 1];
      obj.bitmap.y = MTLG.getOptions().height / 2 + y[MTLG.getPlayer(2).position - 1];
      obj.bitmap.rotation = rotation[MTLG.getPlayer(2).position - 1];
      MTLG.getStageContainer().addChild(obj.bitmap)
    }, { width: MTLG.getOptions().height / 3, showText: false, fontColor: 'white' })
  }

  if (MTLG.getPlayer(3)) {
    MTLG.utils.qrcode.createQRCode(await create_qr_link(MTLG.getPlayer(3).pseudonym), (obj) => {
      obj.bitmap.x = MTLG.getOptions().width / 2 + x[MTLG.getPlayer(3).position - 1];
      obj.bitmap.y = MTLG.getOptions().height / 2 + y[MTLG.getPlayer(3).position - 1];
      obj.bitmap.rotation = rotation[MTLG.getPlayer(3).position - 1];
      MTLG.getStageContainer().addChild(obj.bitmap)
    }, { width: MTLG.getOptions().height / 3, showText: false, fontColor: 'white' })
  }

  var player1 = false;
  var player2 = false;
  var player3 = false;
  var player4 = false;

  var readyButton1a = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  readyButton1a.regX = 100 / 2;
  readyButton1a.regY = 100 / 2;
  readyButton1a.rotation = 180;
  readyButton1a.x = MTLG.getOptions().width / 2 - 210;
  readyButton1a.y = MTLG.getOptions().height / 2 - 50;
  stage.addChild(readyButton1a);

  readyButton1a.on("pressup", function (evt) {
    readyButton1a.text = "";
    player1 = true;
    logEvent("voted", 1)
    if (player1 && player2 && player3 && player4) {
      logEvent("triggered", 1);
      big5Colors();
    }
  });


  var readyButton2a = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  readyButton2a.regX = 100 / 2;
  readyButton2a.regY = 100 / 2;
  readyButton2a.rotation = 180;
  readyButton2a.x = MTLG.getOptions().width / 2 + 750;
  readyButton2a.y = MTLG.getOptions().height / 2 - 50;
  stage.addChild(readyButton2a);

  readyButton2a.on("pressup", function (evt) {
    readyButton2a.text = "";
    player2 = true;
    logEvent("voted", 2)
    if (player1 && player2 && player3 && player4) {
      logEvent("triggered", 2);
      big5Colors();
    }
  });


  var readyButton3a = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  readyButton3a.regX = 100 / 2;
  readyButton3a.regY = 100 / 2;
  readyButton3a.rotation = 0;
  readyButton3a.x = MTLG.getOptions().width / 2 - 750;
  readyButton3a.y = MTLG.getOptions().height / 2 + 40;
  stage.addChild(readyButton3a);

  readyButton3a.on("pressup", function (evt) {
    readyButton3a.text = "";
    player3 = true;
    logEvent("voted", 3)
    if (player1 && player2 && player3 && player4) {
      logEvent("triggered", 3);
      big5Colors();
    }
  });


  var readyButton4a = new createjs.Text("✅", "bold 80px Lucida Console", MTLG.getSettings().default.main_color);
  readyButton4a.regX = 100 / 2;
  readyButton4a.regY = 100 / 2;
  readyButton4a.rotation = 0;
  readyButton4a.x = MTLG.getOptions().width / 2 + 210;
  readyButton4a.y = MTLG.getOptions().height / 2 + 40;
  stage.addChild(readyButton4a);

  readyButton4a.on("pressup", function (evt) {
    readyButton4a.text = "";
    player4 = true;
    logEvent("voted", 4)
    if (player1 && player2 && player3 && player4) {
      logEvent("triggered", 4);
      big5Colors();
    }
  });

}

function surveyColor(objc) {
  console.log("Evaluation in process.")
  var color;
  var maxValue = 0;
  var maxKey;
  for (const [key, value] of Object.entries(objc)) {
    if (value > maxValue) {
      maxKey = key;
      maxValue = value;
    }
  }
  console.log(maxKey)
  console.log(maxValue)
  switch (maxKey) {
    case 'E':
      color = 'purple';
      break;
    case 'V':
      color = 'yellow';
      break;
    case 'G':
      color = 'blue';
      break;
    case 'N':
      color = 'green';
      break;
    case 'O':
      color = 'orange';
      break;
    default:
      color = 'red';
      break;
  }
  console.log(color);
  return color;
}

function big5Colors() {
  //Set colors depending on results of limesurvey "big5"
  //https://mtlg.elearn.rwth-aachen.de/ls_connector/big5/link/pseudonym

  const init = {
    method: 'GET',
    mode: 'no-cors',
  }

  var _newDefaultSettings = {
    default: {
      color_p1: 'green',
      color_p2: 'green',
      color_p3: 'green',
      color_p4: 'green'
    }
  };

  players_loaded = 0
  MTLG.getPlayers().forEach(player => {
    const url0 = 'https://mtlg.elearn.rwth-aachen.de/ls_connector/big5/results/' + player.pseudonym;
    //const request0 = new Request(url0, init);
    var survey0;

    let answers = axios.get(url0)
      .then(function (response) {
        console.log("Limesurvey import completed.")
        console.log(response);
        survey0 = response;
        if (survey0.data != 'undefined') {
          var score = JSON.parse(survey0.data).score
          _newDefaultSettings.default['color_p' + player.position] = surveyColor(score)
          //console.log(JSON.parse(survey0.data).score);
        }
        players_loaded++;
        if (players_loaded >= MTLG.getPlayers().length) {
          MTLG.loadSettings(_newDefaultSettings);
          console.log(MTLG.getSettings().default.color_p1)
          console.log(MTLG.getSettings().default.color_p2)
          console.log(MTLG.getSettings().default.color_p3)
          console.log(MTLG.getSettings().default.color_p4)
          MTLG.lc.levelFinished({
            nextLevel: "level1"
          });
        }
        return survey0
      })
      .catch(function (error) {
        console.log(error);
        players_loaded++;
        if (players_loaded >= MTLG.getPlayers().length) {
          MTLG.loadSettings(_newDefaultSettings);
          MTLG.lc.levelFinished({
            nextLevel: "level1"
          });
        }
      });



  })

  /*
  if (MTLG.getPlayer(0)) {
    const url0 = 'https://mtlg.elearn.rwth-aachen.de/ls_connector/big5/results/' + MTLG.getPlayer(0).pseudonym;
    const request0 = new Request(url0, init);
    var survey0;

    let answers = axios.get(url0)
      .then(function (response) {
        console.log("Limesurvey import completed.")
        console.log(response);
        survey0 = response;
      })
      .catch(function (error) {
        console.log(error);
      });

  JSON.parse(answers.data)



  }

  if (MTLG.getPlayer(1)) {
    const url1 = 'https://mtlg.elearn.rwth-aachen.de/ls_connector/big5/results/' + MTLG.getPlayer(1).pseudonym;
    const request1 = new Request(url1, init);
    var survey1;
    fetch(request1).then(function (response) {
      console.log("big5", response);
      //return response.json();
    }).then(function (data) {
      survey1 = data;
      console.log("big5", data);
    }).catch(function (e) {
      console.log("big5e", e);
    });
  }

  if (MTLG.getPlayer(2)) {
    const url2 = 'https://mtlg.elearn.rwth-aachen.de/ls_connector/big5/results/' + MTLG.getPlayer(2).pseudonym;
    const request2 = new Request(url2, init);
    var survey2;
    fetch(request2).then(function (response) {
      console.log("big5", response);
      //return response.json();
    }).then(function (data) {
      survey2 = data;
      console.log("big5", data);
    }).catch(function (e) {
      console.log("big5e", e);
    });
  }

  if (MTLG.getPlayer(3)) {
    const url3 = 'https://mtlg.elearn.rwth-aachen.de/ls_connector/big5/results/' + MTLG.getPlayer(3).pseudonym;
    const request3 = new Request(url3, init);
    var survey3;
    fetch(request3).then(function (response) {
      console.log("big5", response);
      //return response.json();
    }).then(function (data) {
      survey3 = data;
      console.log("big5", data);
    }).catch(function (e) {
      console.log("big5e", e);
    });
  }
  */





  console.log(MTLG.getSettings());

  //Start first level

}
