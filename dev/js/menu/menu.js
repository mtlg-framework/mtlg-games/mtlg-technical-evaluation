
function drawMainMenu() {
  // get stage to add objects
  var stage = MTLG.getStageContainer();

  let loginButton = MTLG.utils.uiElements.addButton({
    text: "START",
    sizeX: MTLG.getOptions().width * 0.3,
    sizeY: 150,
  },
    function () {
      MTLG.lc.levelFinished({
        nextLevel: "login"
      });
    }
  );
  loginButton.x = MTLG.getOptions().width / 2;
  loginButton.y = MTLG.getOptions().height / 4;

  let pongButton = MTLG.utils.uiElements.addButton({
    text: "Pong - Controller",
    sizeX: MTLG.getOptions().width * 0.3,
    sizeY: 150,
  },
    function () {
      MTLG.lc.levelFinished({
        nextLevel: "control"
      });
    }
  );
  pongButton.x = MTLG.getOptions().width / 2;
  pongButton.y = MTLG.getOptions().height / 4 + 250;

  let level1 = MTLG.utils.uiElements.addButton({
    text: "1",
    sizeX: 50,
    sizeY: 50,
  },
    function () {
      MTLG.lc.levelFinished({
        nextLevel: "level1"
      });
    }
  );
  level1.x = MTLG.getOptions().width * 3 / 9;
  level1.y = MTLG.getOptions().height / 3 + 500;

  let level2 = MTLG.utils.uiElements.addButton({
    text: "2",
    sizeX: 50,
    sizeY: 50,
  },
    function () {
      MTLG.lc.levelFinished({
        nextLevel: "level2"
      });
    }
  );
  level2.x = MTLG.getOptions().width * 4 / 9;
  level2.y = MTLG.getOptions().height / 3 + 500;

  let level3 = MTLG.utils.uiElements.addButton({
    text: "3",
    sizeX: 50,
    sizeY: 50,
  },
    function () {
      MTLG.lc.levelFinished({
        nextLevel: "level3"
      });
    }
  );
  level3.x = MTLG.getOptions().width * 5 / 9;
  level3.y = MTLG.getOptions().height / 3 + 500;

  let level4 = MTLG.utils.uiElements.addButton({
    text: "4",
    sizeX: 50,
    sizeY: 50,
  },
    function () {
      MTLG.lc.levelFinished({
        nextLevel: "pong"
      });
    }
  );
  level4.x = MTLG.getOptions().width * 6 / 9;
  level4.y = MTLG.getOptions().height / 3 + 500;

  let CKAN = MTLG.utils.uiElements.addButton({
    text: "CKAN-Test",
    sizeX: 200,
    sizeY: 50,
  },
    function () {
      MTLG.lc.levelFinished({
        nextLevel: "ckan"
      });
    }
  );
  CKAN.x = MTLG.getOptions().width / 2;
  CKAN.y = MTLG.getOptions().height / 4 + 450;

  stage.addChild(loginButton);
  stage.addChild(pongButton);

  stage.addChild(level1);
  stage.addChild(level2);
  stage.addChild(level3);
  stage.addChild(level4);

  stage.addChild(CKAN);
}
