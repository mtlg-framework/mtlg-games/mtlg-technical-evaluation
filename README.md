# mtlg-technical-evaluation
 
- Level 0: login & limesurvey
- Level 1: local interaction
- Level 2: simultaneous interaction
- Level 3: drawing interaction
- Level 4: distributed displays
 
## login & limesurvey
 
After pressing start, the login procedure starts. Complete the login and hit the green button. If everybody is ready, every user who is logged in can participate in a survey by scanning the QR Code. The answers during this survey determine the color of each player during the following game.
 
## local interaction
 
Every user has to drag his box into the big red box exactly `dragLimit` times. (`game.settings.js`)
 
## simultaneous interaction
 
The time span from the first successful drag to the last successful drag of every user has to be less or equal to `timeLimit` seconds. (`game.settings.js`)
 
## drawing interaction
 
There will be shapes displayed which every user has to reproduce by drawing. The group gets three tries, every try gets a little easier.
 
## PONG

Playing Pong via Distributed Displays. Every user scans the QR Code and hits _Pong - Controller_. Every player has to be logged in.
 
## logging

Every move of every player gets logged in detail in two ways.

1. http://lrs.elearn.rwth-aachen.de/ via the xAPI module, every logging event gets transmitted imminently
2. log file via local storage, file downloads automatically as soon as PONG is finished
 
## game.settings.js
 
Personalize the game by editing the `dev/manifest/game.settings.js` file. For example, the win limit of level 4 (PONG) can be changed.
